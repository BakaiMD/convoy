#pragma once
#include "BaseShip.h"
#include "GuardShip.h"
#include "BaseShip.h"
#include "MilitaryShip.h"
#include <iostream>
#include "TableElem.h"

typedef vector<TableElem>::iterator TableShipIterator;
typedef vector<TableElem>::const_iterator const_TableShipIterator;

/*! \brief ������������ �����, ������������� � ���� ������� 
\details ������� ������� ������ ������ TableElem

����� ��� ����������� ������� ����������� ������������ ����� STL -vector � ��� ��������� */
class TableShip
{
public:
	/*! \brief ������� ����������� */
	TableShip();
	/*! \brief ���������������� �����������*/
	TableShip(const TableElem&);
	/*! \brief ���������� �����������*/
	TableShip(TableShip&);
	~TableShip();
	/*! \brief ���������� �������� ����������� �� ������ �������
	\return vector<TableElem>::iterator */
	TableShipIterator begin();
	/*! \brief ���������� �������� ����������� �� ����� �������
	\return vector<TableElem>::iterator*/
	TableShipIterator end();
	/*! \brief ���������� ��������� �� ����� �� ��� ��������
	\param �������� �����
	\throw ����� � ������ ��������� �� ���������� � �������*/
	BaseShip* returnShip(string) const;
	/*! \brief ���������� ���������� ��������� � �������*/
	unsigned int getNumOfShips() const;
	/*! \brief ��������� ����� � ������� 
	\param ��������� ������� ���������� ��������� �� ����� � ������ �������� �������
	\throw �������� ����� �� ���������*/
	TableShip& push_back(const TableElem&);
	/*! \brief ������� ����� �� ������� 
	\param \������ �������� �������
	\throw �������� ����� �� ������� � �������*/
	TableShip& deleteElem(string);
	/*! \brief ������������� �������� ������������*/
	TableShip& operator =(TableShip&);
	/*! \brief ����� ������������ ���������� ������*/
	void erase();
	/*! \brief ������ ����� ���������� ������� */
	void print() const;
	/*! \brief ���������� ������� � ����(�������� ��� ���������)*/
	void savebin(FILE*f);
	/*! \brief �������� ������� �� �����(�������� ��� ���������)*/
	void loadbin(FILE*f);
	/*! \brief  �������� �� ������� �������
	\return bool true - ������� ����� , false - ������� �� �����*/
	bool empty() { return vectorOfElements.empty(); };
private:
	vector<TableElem> vectorOfElements;
};

