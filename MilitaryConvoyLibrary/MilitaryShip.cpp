#include "stdafx.h"
#include "MilitaryShip.h"
#include <iostream>
using std::cout;
using std::endl;


MilitaryShip::MilitaryShip():GuardShip()
{
	setCargoWeight(0);
}

MilitaryShip::MilitaryShip(Captain cap, string name, double speed, double displacement, int passengers,  double cargo,int k) :
	GuardShip(cap, name, speed, displacement, passengers,k)
{
	setCargoWeight(cargo);
}

MilitaryShip::~MilitaryShip()
{
}


double MilitaryShip::getCargoWeight() const
{
	return cargo_weight;
}

void MilitaryShip::setCargoWeight(double weight)
{
	if (weight >= 0)
		cargo_weight = weight;
	setMaxPossibleSpeed(lawOfMaxPossibleSpeed());
}


double MilitaryShip::lawOfMaxPossibleSpeed()
{
	double MAXPossibleSpeed;
	if(cargo_weight<=1000)
	MAXPossibleSpeed = getMaxSpeed();
	if (cargo_weight > 1000 && cargo_weight <= 5000)
		MAXPossibleSpeed = 0.75*getMaxSpeed();
	if(cargo_weight>5000 && cargo_weight<=10000)
		MAXPossibleSpeed = 0.5*getMaxSpeed();
	if(cargo_weight>10000)
		MAXPossibleSpeed = 0.25*getMaxSpeed();
	return MAXPossibleSpeed;
}

void MilitaryShip::setMaxPossibleSpeed(double PossibleSpeed)
{
	if (PossibleSpeed <= lawOfMaxPossibleSpeed())
		setSpeed(PossibleSpeed);
}

int MilitaryShip::getTypeOfShip() const
{
	return MILITARY;
}

ostream& MilitaryShip::writeln(ostream&os)
{
	GuardShip::writeln(os);
	os << cargo_weight << endl;
	
	return os;
}
istream& MilitaryShip::readln(istream& is)
{
	GuardShip::readln(is);
	is >> cargo_weight;
	if (cargo_weight < 0)
		is.setstate(is.failbit);
	setMaxPossibleSpeed(lawOfMaxPossibleSpeed());
	return is;
}



void  MilitaryShip::print() const
{
	cout << "MILITARY SHIP" << endl;
	GuardShip::print();
	cout << "CARGO WEIGHT " << cargo_weight << " kg" << endl;
}

MilitaryShip & MilitaryShip::operator =(MilitaryShip &obj)
{
	GuardShip::operator=(obj);
	setCargoWeight(obj.cargo_weight);
	return *this;

}

MilitaryShip* MilitaryShip:: clone() const
{
	return new MilitaryShip(*this);
}

void MilitaryShip::savebin(FILE *f)
{
	GuardShip::savebin(f);
	fwrite(&cargo_weight, sizeof(double), 1, f);
	return;
}

void MilitaryShip::loadbin(FILE *f)
{
	GuardShip::loadbin(f);
	fread(&cargo_weight, sizeof(double), 1, f);
	setMaxPossibleSpeed(lawOfMaxPossibleSpeed());
	return;
}