#include "stdafx.h"
#include "TableShip.h"
#include <iostream>


using std::cout;
using std::endl;

TableShip::TableShip()
{
}

TableShip::TableShip(const TableElem& newElem)
{
	vectorOfElements.push_back(newElem);
	vectorOfElements.begin()->busy = 1;
}

TableShip::TableShip(TableShip& copyTable)
{
	vectorOfElements.reserve(copyTable.getNumOfShips());
	TableShipIterator it;
	for (it = copyTable.begin(); it != copyTable.end(); ++it)
	{
		vectorOfElements.push_back(*it);
	}
}

TableShip::~TableShip()
{
	vectorOfElements.clear();
}


TableShipIterator TableShip::begin()
{
	return vectorOfElements.begin();
}


TableShipIterator TableShip::end()
{
	return vectorOfElements.end();
}

BaseShip* TableShip::returnShip(string calling) const
{
	vector<TableElem>::const_iterator it;
	for (it = vectorOfElements.begin(); it != vectorOfElements.end(); ++it)
		if (it->call_letter == calling)
			return it->ship;
	return NULL;
}

unsigned int TableShip::getNumOfShips() const
{
	return vectorOfElements.size();
}


TableShip&  TableShip::push_back(const TableElem& newElem)
{
	vector<TableElem>::iterator it;
	for (it = vectorOfElements.begin(); it != vectorOfElements.end(); ++it)
		if (it->call_letter == newElem.call_letter) 
			throw invalid_argument("Ship with this call letter already exists");
	vectorOfElements.reserve(getNumOfShips() + 1);
	vectorOfElements.push_back(newElem);
	return *this;
}


TableShip& TableShip::deleteElem(string delElem)
{
	vector<TableElem>::iterator it;
	for (it = vectorOfElements.begin(); it != vectorOfElements.end(); ++it)
		if (it->call_letter == delElem)
		{
			it->erase();
			*it = vectorOfElements[vectorOfElements.size() - 1];
			vectorOfElements.resize(vectorOfElements.size() - 1);
			return *this;
		}
	throw "SHIP WITH THIS CALL LETTER DOESN'T EXIST";
	return *this;
}

void TableShip::print() const
{
	cout << "FULL INFORMATION ABOUT SHIPS:" << endl;
	vector<TableElem>::const_iterator it;
	for (it = vectorOfElements.begin(); it != vectorOfElements.end(); ++it)
	{
		cout<<endl << "CALL LETTER: "<<it->call_letter<<endl;
		it->ship->print();
	}
}


TableShip& TableShip::operator =( TableShip&copy)
{
	vectorOfElements.clear();
	vectorOfElements.reserve(copy.getNumOfShips());
	for (auto it = copy.begin(); it != copy.end(); ++it)
	{
		vectorOfElements.push_back(*it);
	}
	return *this;
}

void TableShip::erase()
{
	for (auto it = begin(); it != end(); ++it)
		it->erase();
}


void TableShip::savebin(FILE *f)
{
	size_t sz;
	sz = vectorOfElements.size();
	fwrite(&sz, sizeof(size_t), 1, f);
	for (auto it = vectorOfElements.begin(); it != end(); ++it)
		it->savebin(f);
	return;
}

void TableShip::loadbin(FILE *f)
{
	size_t sz;
	fread(&sz, sizeof(size_t), 1, f);
	vectorOfElements.reserve(sz);
		for (size_t i = 0; i < sz; ++i)
		{
			TableElem elem;
			elem.loadbin(f);
			vectorOfElements.push_back(elem);
		}
		return;
}