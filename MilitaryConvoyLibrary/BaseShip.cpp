#include "stdafx.h"
#include <iostream>
#include "BaseShip.h"

using std::cout;
using std::cin;
using std::endl;


BaseShip::BaseShip()
{
	Captain unknown;
	captain = unknown;
	max_speed = 0;
	ship_displacement = 0;
	num_passengers = 0;
}


BaseShip::BaseShip(Captain cap, string name, double MAXspeed, double displacement, int passengers): max_speed(MAXspeed)
{
	setCaptain(cap);
	setShipName(name);
	setShipDisplacement(displacement);
	setNumPassengers(passengers);
	setMaxSpeed(MAXspeed);
}

BaseShip::~BaseShip()
{
}

BaseShip::BaseShip(const BaseShip& CopyShip)
{
	setCaptain(CopyShip.captain);
	setShipName (CopyShip.ship_name);
	setShipDisplacement(CopyShip.ship_displacement);
	setNumPassengers (CopyShip.num_passengers);
	setMaxSpeed(CopyShip.max_speed);
}

Captain BaseShip:: getCaptain() const 
{
	return captain;
}


void BaseShip::setCaptain(const Captain& cap)
{
	captain = cap;
}

double BaseShip::getMaxSpeed() const
{
	return max_speed;
}

void BaseShip::setMaxSpeed(double MAXspeed) 
{
	if (MAXspeed >= 0) {
		max_speed = MAXspeed;
	}
	else
		throw invalid_argument("MAX SPEED MUST BE POSITIVE");
}

void BaseShip::setSpeed(double fitSpeed)
{
	speed = fitSpeed;
}

double BaseShip::getPossibleMaxSpeed() const
{
	return speed;
}

string BaseShip::getShipName() const
{
	return ship_name;
}

void BaseShip::setShipName(string name) 
{
	ship_name.clear();
	ship_name = name;
}

double BaseShip::getShipDisplacement() const
{
	return ship_displacement;
}

void BaseShip::setShipDisplacement(double displacement)
{
	if(displacement>=0)
	ship_displacement = displacement;
	else
		throw invalid_argument("SHIP DISPLACEMENT MUST BE POSITIVE");
}

int BaseShip::getNumPassengers() const
{
	return num_passengers;
}

void BaseShip::setNumPassengers(int passengers)
{
	if(passengers >=0)
	num_passengers = passengers;
	else
		throw invalid_argument("NUMBER OF PASSENGERS CANNOT BE NEGATIVE");
}

istream&operator >>(istream& is, BaseShip& newShip)
{
	newShip.readln(is);
	return is;
}


ostream&operator <<(ostream& os, BaseShip& requiredShip)
{
	requiredShip.writeln(os);
	return os;
}

ostream& BaseShip::writeln(ostream& os)
{
	os << ship_name << endl;
	os << captain.FirstName << endl << captain.LastName << endl;
	os << captain.captainship << endl << captain.experience << endl;
	os << speed << endl;
	os << ship_displacement << endl;
	os << num_passengers << endl;
	return os;
}




void BaseShip::print() const
{
	cout <<"FULL INFORMATION:"<<endl<< "SHIP " << ship_name << endl;
	cout << "CAPTAIN:" << endl << "FIRST NAME " << captain.FirstName << endl << "LAST NAME " << captain.LastName << endl;
	cout << "CAPTAINSHIP " << captain.captainship << endl << "EXPERIENCE " << captain.experience << " YEARS" << endl;
	cout << "SPEED OF SHIP " << speed << endl;
	cout << "SHIP DISPLACEMENT " << ship_displacement << endl;
	cout << "NUMBER OF PASSENGERS " << num_passengers << endl;
}


BaseShip & BaseShip:: operator =(const BaseShip &CopyShip)
{
	setCaptain(CopyShip.captain);
	setShipName(CopyShip.ship_name);
	setShipDisplacement(CopyShip.ship_displacement);
	setNumPassengers(CopyShip.num_passengers);
	setMaxSpeed(CopyShip.max_speed);
	return *this;
}


istream& BaseShip::readln(istream& is)
{
	int exper, MAXspeed, displacement, passengers;
	is >> ship_name;
	is >> captain.FirstName;
	is >> captain.LastName;
	is >> captain.captainship;
	is >> exper;
	if (exper < 0)
		is.setstate(is.failbit);
	captain.experience = exper;
	is >> MAXspeed;
	if (MAXspeed<0)
		is.setstate(is.failbit);
	max_speed = MAXspeed;
	setMaxPossibleSpeed(MAXspeed);

	is >> displacement;
	if (displacement < 0)
		is.setstate(is.failbit);
	ship_displacement = displacement;
	is >> passengers;
	if (passengers < 0)
		is.setstate(is.failbit);
	num_passengers = passengers;
	return is;
	}


void BaseShip:: savebin(FILE *f)
{
	int r = getTypeOfShip();
	fwrite(&r ,sizeof(int), 1, f);
	size_t lenght;
	lenght = ship_name.length();
	fwrite(&lenght, sizeof(size_t), 1, f);
	for (auto it = ship_name.begin(); it != ship_name.end(); ++it)
		fwrite(&(*it), sizeof(char), 1, f);
	lenght = captain.FirstName.length();
	fwrite(&lenght, sizeof(int), 1, f);
	for (auto it = captain.FirstName.begin(); it != captain.FirstName.end(); ++it)
		fwrite(&(*it), sizeof(char), 1, f);
	lenght = captain.LastName.length();
	fwrite(&lenght, sizeof(int), 1, f);
	for (auto it = captain.LastName.begin(); it != captain.LastName.end(); ++it)
		fwrite(&(*it), sizeof(char), 1, f);
	lenght = captain.captainship.length();
	fwrite(&lenght, sizeof(int), 1, f);
	for (auto it = captain.captainship.begin(); it != captain.captainship.end(); ++it)
		fwrite(&(*it), sizeof(char), 1, f);
	fwrite(&captain.experience, sizeof(int), 1, f);
	fwrite(&max_speed, sizeof(double), 1, f);
	fwrite(&ship_displacement, sizeof(double), 1, f);
	fwrite(&num_passengers, sizeof(int), 1, f);
	return;
}


int BaseShip::freadType(FILE *f)
{
	int Type;
	fread(&Type, sizeof(int), 1, f);
	return Type;
}

void BaseShip::loadbin(FILE*f)
{
	size_t lenght;
	fread(&lenght, sizeof(size_t), 1, f);
	for (size_t i = 0; i < lenght; ++i) 
	{
		char symbol;
		fread(&symbol, sizeof(char), 1, f);
		ship_name = ship_name + symbol;
	}
	fread(&lenght, sizeof(int), 1, f);
	for (size_t i = 0; i < lenght; ++i)
	{
		char symbol;
		fread(&symbol, sizeof(char), 1, f);
		captain.FirstName = captain.FirstName + symbol;
	}
	fread(&lenght, sizeof(int), 1, f);
	for (size_t i = 0; i < lenght; ++i)
	{
		char symbol;
		fread(&symbol, sizeof(char), 1, f);
		captain.LastName = captain.LastName + symbol;
	}
	fread(&lenght, sizeof(int), 1, f);
	for (size_t i = 0; i < lenght; ++i)
	{
		char symbol;
		fread(&symbol, sizeof(char), 1, f);
		captain.captainship = captain.captainship + symbol;
	}
	fread(&captain.experience, sizeof(int), 1, f);
	fread(&max_speed, sizeof(double), 1, f);
	fread(&ship_displacement, sizeof(double), 1, f);
	fread(&num_passengers, sizeof(int), 1, f);
	return;
}

