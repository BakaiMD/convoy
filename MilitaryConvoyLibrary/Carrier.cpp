#include "stdafx.h"
#include "Carrier.h"
#include "BaseShip.h"
#include <iostream>

using std::cout;
using std::cin;
using std::endl;

Carrier::Carrier():BaseShip()
{
	setCargoWeight(0);
	setMaxPossibleSpeed(0);
}


Carrier::Carrier(Captain cap, string name, double speed, double displacement, int passengers, double cargo) :
	BaseShip(cap, name, speed, displacement, passengers)
{
	setCargoWeight(cargo);
	setMaxPossibleSpeed(lawOfMaxPossibleSpeed());
}

Carrier::Carrier(const Carrier&re):BaseShip(re)
{
	setCargoWeight(re.cargo_weight);
	setMaxPossibleSpeed(lawOfMaxPossibleSpeed());
}

Carrier::~Carrier()
{
}


Carrier* Carrier::clone() const
{
	return new Carrier(*this);
}

double Carrier::getCargoWeight() const
{
	return cargo_weight;
}

void Carrier::setCargoWeight(double weight)
{
	if(weight>=0)
	cargo_weight = weight;
	setMaxPossibleSpeed(lawOfMaxPossibleSpeed());
}

double Carrier:: lawOfMaxPossibleSpeed()
{
	double MAXPossibleSpeed;
	if(cargo_weight<=500)
	MAXPossibleSpeed = getMaxSpeed();
	if (cargo_weight > 500 && cargo_weight<=2000)
		MAXPossibleSpeed = 0.75*getMaxSpeed();
	if (cargo_weight > 2000 && cargo_weight <= 7000)
		MAXPossibleSpeed = 0.5*getMaxSpeed();
	if (cargo_weight > 7000 && cargo_weight <= 15000)
		MAXPossibleSpeed = 0.25*getMaxSpeed();
	if(cargo_weight>15000)
		MAXPossibleSpeed = 0.1*getMaxSpeed();
	return MAXPossibleSpeed;
}

void Carrier:: setMaxPossibleSpeed(double PossibleSpeed)
{
	if (PossibleSpeed <= lawOfMaxPossibleSpeed())
		setSpeed (PossibleSpeed);
}


void Carrier::print() const
{
	cout << "CARRIER" << endl;
	BaseShip::print();
	cout << "CARGO WEIGHT " << cargo_weight <<" kg"<< endl;
}


ostream& Carrier:: writeln(ostream& os)
{
	BaseShip::writeln(os);
	os << cargo_weight << endl;
	return os;
}

istream& Carrier:: readln(istream& is)
{
	BaseShip::readln(is);
	is >> cargo_weight;
	if (cargo_weight < 0)
		is.setstate(is.failbit);
	setMaxPossibleSpeed(lawOfMaxPossibleSpeed());
	return is;
}


Carrier & Carrier:: operator =(const Carrier &obj)
{
	BaseShip::operator=(obj);
	setCargoWeight(obj.cargo_weight);
	return *this;
}

int Carrier::getTypeOfShip() const
{
	return CARRIER;
}

void Carrier::savebin(FILE*f)
{
	BaseShip::savebin(f);
	fwrite(&cargo_weight, sizeof(double), 1, f);
	return;
}

void Carrier::loadbin(FILE *f)
{
	BaseShip::loadbin(f);
	fread(&cargo_weight, sizeof(double), 1, f);
	return;
}