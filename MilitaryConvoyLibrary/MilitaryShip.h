#pragma once
#include "GuardShip.h"
#include "Carrier.h"

/*! \brief �������� ����� , ����������� ������ �������� ����������
\details � �������� ����� ����������� ���������� � ����� ����� � ������ � ����������� ����������*/
class MilitaryShip :
	public GuardShip
{
public:
	/*! \brief ������� ����������� , ������ BaseShip*/
	MilitaryShip();
	/*! \brief ���������������� ����������� , ������ BaseShip*/
	MilitaryShip(Captain cap, string name, double speed, double displacement, int passengers,   double cargo,int k);
	~MilitaryShip();
	double getCargoWeight() const;
	void setCargoWeight(double);
	/*! \brief ����� ����������� ����������� �������� ����� �� ��� ��������
	\return ����������� ��������� �������� ��� ������ ��������*/
	double lawOfMaxPossibleSpeed();
	/*! \throw ���� ��������������� �������� max > ������������ ��������� �������� ������������� �����*/
	virtual void setMaxPossibleSpeed(double max);
	virtual void  print() const;
	virtual int getTypeOfShip() const;
	virtual ostream& writeln(ostream&);
	virtual istream& readln(istream&);
	virtual MilitaryShip & operator =(MilitaryShip &obj);
	MilitaryShip* clone() const;
	virtual void savebin(FILE *f);
	virtual void loadbin(FILE*f);
private:
	double cargo_weight;
};

