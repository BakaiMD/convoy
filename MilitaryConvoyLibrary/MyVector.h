
#pragma once
#include <string>

	template <class MyType>
class MyVector
{
	MyType* vbegin;
	MyType* vend;
	MyType* dend;
public:
	class const_iterator;
	class iterator;
	MyVector();
	~MyVector();
	void resize(int S);
	void clear();
	void push_back(MyType T);
	const int size() const;
	const iterator& erase(const iterator i);
	const iterator& insert(const iterator i, const MyType E);
	const const_iterator& erase(const const_iterator i);
	const const_iterator& insert(const const_iterator i, const MyType E);
	const const_iterator& cbegin() const;
	const const_iterator& cend() const;
	const iterator& begin();
	const iterator& end();
};

template <class MyType>
class MyVector<MyType>::iterator
{

public:
	MyType* ptr;
	iterator();
	iterator(MyType* p);
	~iterator();
	MyType& operator *() const;
	const bool operator ==(const iterator T) const;
	const bool operator !=(const iterator T) const;
	const bool operator ==(const const_iterator T) const;
	const bool operator !=(const const_iterator T) const;
	const iterator operator =(const const_iterator T);
	const iterator operator ++();
	const iterator operator --();
};

template <class MyType>
class MyVector<MyType>::const_iterator
{

public:
	MyType* ptr;
	const_iterator();
	const_iterator(MyType* p);
	~const_iterator();
	const MyType operator *() const;
	const bool operator ==(const iterator T) const;
	const bool operator !=(const iterator T) const;
	const bool operator ==(const const_iterator T) const;
	const bool operator !=(const const_iterator T) const;
	const const_iterator operator =(const iterator T);
	const const_iterator operator ++();
	const const_iterator operator --();
};







template <class DCC>
MyVector<DCC>::MyVector() :vbegin(nullptr), vend(nullptr), dend(nullptr)
{
}

template <class DCD>
MyVector<DCD>::~MyVector()
{
	delete []vbegin;
	vbegin = nullptr;
	vend = nullptr;
	dend = nullptr;
}

template <class MyType>
void MyVector<MyType>::resize(int S)
{
	int DS = dend - vbegin;
	if (S < DS)
	{
		throw "Illegal size";
	}
	if (S == vend - vbegin)
	{
		return;
	}
	MyType* B = vbegin;
	vbegin = new MyType[S + 1];
	memcpy_s(vbegin, (S + 1)*sizeof(MyType), B, DS*sizeof(MyType));
	vend = vbegin + S;
	dend = vbegin + DS;
	delete B;
	return;
}

template <class MyType>
const typename MyVector<MyType>::const_iterator& MyVector<MyType>::cbegin() const
{
	typename MyVector<MyType>::const_iterator i(vbegin);
	return i;
}

template <class MyType>
const typename MyVector<MyType>::const_iterator& MyVector<MyType>::cend() const
{
	typename MyVector<MyType>::const_iterator i;
	if (dend == nullptr)
		i.ptr = nullptr;
	else
		i.ptr = dend;
	return i;
}

template <class MyType>
const typename MyVector<MyType>::iterator& MyVector<MyType>::begin()
{
	typename MyVector<MyType>::iterator i(vbegin);
	return i;
}

template <class MyType>
const typename MyVector<MyType>::iterator& MyVector<MyType>::end()
{
	typename MyVector<MyType>::iterator i;
	if (dend == nullptr)
		i.ptr = nullptr;
	else
		i.ptr = dend;
	return i;
}

template<class MyType>
void MyVector<MyType>::push_back(MyType T)
{
	if (vend == dend)
		resize(2 + (vend - vbegin)*1.2);
	*dend = T;
	dend++;
}

template<class MyType>
const typename MyVector<MyType>::iterator& MyVector<MyType>::erase(const typename MyVector<MyType>::iterator I)
{
	typename MyVector<MyType>::iterator i = begin();
	while (i != end() && i != I)
	{
		i++;
	}
	if (i == end())
		return I;
	memcpy_s(i.ptr, (dend - i.ptr)*sizeof(MyType), i.ptr + 1, sizeof(MyType)*(dend - i.ptr));
	dend--;
	return i;
}

template<class MyType>
const typename MyVector<MyType>::iterator& MyVector<MyType>::insert(const typename MyVector<MyType>::iterator I, const MyType E)
{
	MyType* i = vbegin;
	typename MyVector<MyType>::iterator r;
	int S;
	while (i != vend && i != I.ptr)
	{
		i++;
	}
	if (i != I.ptr)
		throw "Illegal place";
	S = i - vbegin;
	if (dend == vend)
		resize(2 + (vend - vbegin)*1.2);
	i = vbegin + S;
	memcpy_s(i + 1, sizeof(MyType)*(vend - i + 1), i, sizeof(MyType)*(dend - i + 1));
	dend++;
	*i = E;
	r.ptr = i;
	return r;
}

template<class MyType>
const typename MyVector<MyType>::const_iterator& MyVector<MyType>::erase(const typename MyVector<MyType>::const_iterator I)
{
	MyType* i = vbegin;
	typename MyVector<MyType>::const_iterator r;
	while (i != *end() && i != *I)
	{
		i++;
	}
	if (i == *end())
		return I;
	memcpy_s(i, sizeof(MyType)*(dend - i), i + 1, sizeof(MyType)*(dend - i));
	dend--;
	r.ptr = i;
	return r;
}

template<class MyType>
const typename MyVector<MyType>::const_iterator& MyVector<MyType>::insert(const typename MyVector<MyType>::const_iterator I, const MyType E)
{
	MyType* i = vbegin;
	typename MyVector<MyType>::const_iterator r;
	int S;
	while (i != vend && i != I.ptr)
	{
		i++;
	}
	if (i != I.ptr)
		throw "Illegal place";
	S = i - vbegin;
	if (dend == vend)
		resize(2 + (vend - vbegin)*1.2);
	i = vbegin + S;
	memcpy_s(i + 1, sizeof(MyType)*(vend - i + 1), i, (dend - i + 1)*sizeof(MyType));
	dend++;
	*i = E;
	i++;
	r.ptr = i;
	return r;
}

template<class MyType>
void MyVector<MyType>::clear()
{
	delete vbegin;
	vbegin = nullptr;
	vend = nullptr;
	dend = nullptr;
	return;
}

template<class MyType>
const int MyVector<MyType>::size() const
{
	return dend - vbegin;
}








template <class MyType>
MyVector<MyType>::const_iterator::const_iterator() :ptr(nullptr)
{
}


template <class MyType>
MyVector<MyType>::const_iterator::~const_iterator()
{
}

template <class MyType>
const MyType MyVector<MyType>::const_iterator::operator *() const
{
	return *ptr;
}

template <class DIC>
MyVector<DIC>::iterator::iterator() :ptr(nullptr)
{
}


template <class MyType>
MyVector<MyType>::iterator::~iterator()
{
}


template <class DIA>
DIA& MyVector<DIA>::iterator::operator *() const
{
	return *ptr;
}

template <class MyType>
MyVector<MyType>::iterator::iterator(MyType* p) :ptr(p)
{
}

template <class MyType>
MyVector<MyType>::const_iterator::const_iterator(MyType* p) : ptr(p)
{
}

template <class MyType>
const bool MyVector<MyType>::iterator::operator ==(const typename MyVector<MyType>::iterator T) const
{
	return (T.ptr == ptr);
}

template <class MyType>
const bool MyVector<MyType>::iterator::operator !=(const typename MyVector<MyType>::iterator T) const
{
	return !(T.ptr == ptr);
}

template <class MyType>
const bool MyVector<MyType>::iterator::operator ==(const typename MyVector<MyType>::const_iterator T) const
{
	return (T.ptr == ptr);
}

template <class MyType>
const bool MyVector<MyType>::iterator::operator !=(const typename MyVector<MyType>::const_iterator T) const
{
	return !(T.ptr == ptr);
}

template <class MyType>
const typename MyVector<MyType>::iterator MyVector<MyType>::iterator::operator = (const typename MyVector<MyType>::const_iterator T)
{
	ptr = T.ptr;
	return *this;
}


template <class MyType>
const bool MyVector<MyType>::const_iterator::operator ==(const typename MyVector<MyType>::iterator T) const
{
	return (T.ptr == ptr);
}

template <class MyType>
const bool MyVector<MyType>::const_iterator::operator !=(const typename MyVector<MyType>::iterator T) const
{
	bool x = nullptr == nullptr;
	return !(T.ptr == ptr);
}

template <class MyType>
const bool MyVector<MyType>::const_iterator::operator ==(const typename MyVector<MyType>::const_iterator T) const
{
	return (T.ptr == ptr);
}

template <class MyType>
const bool MyVector<MyType>::const_iterator::operator !=(const typename MyVector<MyType>::const_iterator T) const
{
	return !(T.ptr == ptr);
}

template <class MyType>
const typename MyVector<MyType>::const_iterator MyVector<MyType>::const_iterator::operator = (const typename MyVector<MyType>::iterator T)
{
	ptr = T.ptr;
	return *this;
}

template <class MyType>
const typename MyVector<MyType>::iterator MyVector<MyType>::iterator::operator ++()
{
	ptr++;
	return *this;
}

template <class MyType>
const typename MyVector<MyType>::const_iterator MyVector<MyType>::const_iterator::operator ++()
{
	ptr++;
	return *this;
}

template <class MyType>
const typename MyVector<MyType>::iterator MyVector<MyType>::iterator::operator --()
{
	ptr--;
	return *this;
}

template <class MyType>
const typename MyVector<MyType>::const_iterator MyVector<MyType>::const_iterator::operator --()
{
	ptr--;
	return *this;
}

