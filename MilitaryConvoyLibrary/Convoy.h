#pragma once



#include "BaseShip.h"
#include "GuardShip.h"
#include "BaseShip.h"
#include "MilitaryShip.h"
#include <iostream>
#include "TableShip.h"


/*! \brief �����,����������� ��������� ������ ������
\details �������� ��������� ����������:

������� ���������� ������

���������� � ����������� �������

����� �����������

����� ����������*/
class Convoy
{
public:
	/*! \brief ������� �����������*/
	Convoy();
	~Convoy();
	/*! \brief ���������� �����������*/
	Convoy(const Convoy&);
	/*! \brief ���������������� �����������*/
	Convoy(TableShip *tabShip, string Cap, string leave, string arrive, double dist);
	int getNumOfSameTypeShips(int type) const;
	/*! \brief ������������� �������� ������������*/
	Convoy& operator =(const Convoy&);
	/*! \brief �������� ����� ����� � ������
	\param ������ ������  TableElem (�������� ����� � ��������� �� ��������� �����)
	\throw �������� ������������ ����� ������ ���� ����������*/
	Convoy& includeNewShip(TableElem&);
	/*! \brief ��������� ����� �� ������
	\param �������� ���������� �����
	\throw ��������� ����� ������ ������� � ������ ������*/
	Convoy& excludeShip(string call_letter);
	/*! \brief ������������ ������������� ����� �� ��������
	\details ����� ���������� ����� ������ �� �������� ����������
	\param �������� �����
	\throw ����� � ������ ��������� �� ������ � ������ ������

	����� � ������ ��������� �� �������� ������������ ������
	\*/
	Convoy& modernCarrierToMilitary(string call_letter);
	/*! \brief ������������� ����� ���������� �� ������ ������*/
	Convoy& redistCargo();
	/*! \brief ������� ����� ����� � ������ ����� �� ������
	\param �������� ���� ������������ ����� ,���������� ������������ ����� �����
	\throw ���� �� ����� �� ������ ������
	
	���������� ����� ����� ������� �������*/
	Convoy& transCargo(string first, string second, double howCargo);
	void setMainCap(string call_letter);
	void setFrom(string leave);
	void setTable(TableShip* tab) { table = tab; };
	void setToward(string toward);
	void setDistance(double dist);
	TableShip* getTable() const { return table; };
	const TableElem getCap() const;
	string getFrom() const;
	string getToward() const;
	double getDistance() const;
	BaseShip* getShip(string call_letter) const;
	Carrier* getCarrier(string call_letter) const;
	GuardShip* getGuardShip(string call_letter) const;
	MilitaryShip* getMilitaryShip(string call_letter) const;
	/*! \brief ������ ����� ���������� � �����*/
	void print() ;
	const bool legalConvoy();
	/*! \brief �������� ������� ��������� �������� ������*/
	double getAverageSpeed() ;
	/*! \brief �������� ������� ����������� �������� ������ */
	double getRealSpeed() ;
	/*! \brief ����� ������� ���������� ������*/
	void erase();
	/*! \brief ���������� ������ ������ � ���� (�������� ��� ���������)*/
	void savebin(FILE*f);
	/*! \brief �������� = ������ ������ �� ����� (�������� ��� ���������)*/
	void loadbin(FILE*f);
private:
	void calculateAverageSpeed() ;
	void calculateRealSpeed();
	void moveWeaponry(BaseShip*);
	TableShip* table;
	TableElem* mainCap;
	string from;
	string toward;
	double distance;
	double averageSpeed;
	double realSpeed;
};

