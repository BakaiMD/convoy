#include "stdafx.h"
#include "TableElem.h"


TableElem::TableElem()
{
	ship = nullptr;
	busy = 0;
}


TableElem::~TableElem()
{
}

TableElem::TableElem(BaseShip*newShip, string calling)
{
	ship = newShip;
	call_letter = calling;
	busy = 0;
}

TableElem::TableElem(BaseShip*newShip, string calling,int k)
{
	ship = newShip;
	call_letter = calling;
	busy = k;
}

TableElem::TableElem(const TableElem& copyShip)
{
	ship = copyShip.ship;
	call_letter =copyShip.call_letter;
	busy = 0;
}

void TableElem::erase()
{
	switch (ship->getTypeOfShip())
	{
	case CARRIER:
		delete (Carrier*)ship;
		break;
	case MILITARY:
		delete (MilitaryShip*)ship;
		break;
	case GUARD:
		delete (GuardShip*)ship;
		break;
	default:
		break;
	}
}


void TableElem::savebin(FILE *f)
{
	ship->savebin(f);
	size_t lenght;
	lenght = call_letter.length();
	fwrite(&lenght, sizeof(size_t), 1, f);
	for (auto it = call_letter.begin(); it != call_letter.end(); ++it)
		fwrite(&(*it), sizeof(char), 1, f);
	return;
}

void TableElem::loadbin(FILE *f)
{
	switch (ship->freadType(f))
	{
	case CARRIER:
	{
		Carrier car;
		car.loadbin(f);
		ship = car.clone();
		break;
	}
	case MILITARY:
	{
		MilitaryShip car;
		car.loadbin(f);
		ship = car.clone();
		break;
	}
	case GUARD:
	{
		GuardShip car;
		car.loadbin(f);
		ship = car.clone();
		break;
	}
	default:
		break;
	}
	size_t sz;
	fread(&sz, sizeof(size_t), 1, f);
	for (size_t k = 0; k < sz; ++k) {
		char ch;
		fread(&(ch), sizeof(char), 1, f);
		call_letter = call_letter + ch;
	}
	return;
}