#pragma once
#include <string>
using std::string;

class Captain
{
public:
	string FirstName;
	string LastName;
	string captainship;
	int experience;
	Captain();
	Captain(string first, string last, string capship, int exper);
	~Captain();
};
