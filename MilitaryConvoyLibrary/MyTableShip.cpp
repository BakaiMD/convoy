#include "stdafx.h"
#include "MyTableShip.h"
#include <iostream>


using std::cout;
using std::endl;

TableShip::TableShip()
{
}

TableShip::TableShip(const TableElem& newElem)
{
	vectorOfElements.push_back(newElem);
}

TableShip::TableShip(TableShip& copyTable)
{
	TableShipIterator it;
	for (it = copyTable.begin(); it != copyTable.end(); ++it)
	{
		vectorOfElements.push_back(*it);
	}
}

TableShip::~TableShip()
{
	vectorOfElements.clear();
}


TableShipIterator TableShip::begin()
{
	return vectorOfElements.begin();
}


TableShipIterator TableShip::end()
{
	return vectorOfElements.end();
}

BaseShip* TableShip::returnShip(string calling) const
{
	
	for (auto it = vectorOfElements.cbegin(); it != vectorOfElements.cend(); ++it)
		if ((*it).call_letter == calling)
			return (*it).ship;
	return NULL;
}

int TableShip::getNumOfShips() const
{
	return vectorOfElements.size();
}


TableShip&  TableShip::push_back(const TableElem& newElem)
{
	for (auto it = vectorOfElements.begin(); it != vectorOfElements.end(); ++it)
		if ((*it).call_letter == newElem.call_letter)
			throw invalid_argument("Ship with this call letter already exists");
	vectorOfElements.push_back(newElem);
	return *this;
}


TableShip& TableShip::deleteElem(string delElem)
{
	for (auto it = vectorOfElements.begin(); it != vectorOfElements.end(); ++it)
		if ((*it).call_letter == delElem)
		{
			(*it).erase();
			auto it1 = vectorOfElements.begin();
			for (int i = 0; i < vectorOfElements.size(); ++i)
				++it1;
			*it = *it1;
			vectorOfElements.resize(vectorOfElements.size() - 1);
			return *this;
		}
	throw "SHIP WITH THIS CALL LETTER DOESN'T EXIST";
	return *this;
}

void TableShip::print() const
{
	cout << "FULL INFORMATION ABOUT SHIPS:" << endl;
	for (auto it = vectorOfElements.cbegin(); it != vectorOfElements.cend(); ++it)
	{
		cout << endl << "CALL LETTER: " << (*it).call_letter << endl;
		(*it).ship->print();
	}
}


TableShip& TableShip::operator =(TableShip&copy)
{
	vectorOfElements.clear();
	for (auto it = copy.begin(); it != copy.end(); ++it)
	{
		vectorOfElements.push_back(*it);
	}
	return *this;
}

void TableShip::erase()
{
	for (auto it = begin(); it != end(); ++it)
		(*it).erase();
}


void TableShip::savebin(FILE *f)
{
	int sz;
	sz = vectorOfElements.size();
	fwrite(&sz, sizeof(int), 1, f);
	for (auto it = vectorOfElements.begin(); it != end(); ++it)
		(*it).savebin(f);
	return;
}

void TableShip::loadbin(FILE *f)
{
	int sz;
	fread(&sz, sizeof(int), 1, f);
	for (int i = 0; i < sz; ++i)
	{
		TableElem elem;
		elem.loadbin(f);
		vectorOfElements.push_back(elem);
	}
	return;
}