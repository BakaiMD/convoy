#include "stdafx.h"
#include "MyConvoy.h"
#include "MilitaryShip.h"

Convoy::Convoy()
{
	table = nullptr;
	distance = 0;
}
Convoy::Convoy(const Convoy&copy)
{
	table = new TableShip;
	*table = *(copy.table);
	mainCap = copy.mainCap;
	from = copy.from;
	toward = copy.toward;
	distance = copy.distance;
}
Convoy::Convoy(TableShip *tabShip, string Cap, string leave, string arrive, double dist)
{
	table = tabShip;
	TableShipIterator it;
	setMainCap(Cap);
	if (leave == arrive)
		throw invalid_argument("ERROR. PLACES CANNOT HAVE SAME NAMES");
	from = leave;
	toward = arrive;
	if (dist == 0)
		throw invalid_argument("IMPOSSIBLE DISTANCE");
	distance = dist;
}
Convoy::~Convoy()
{
}


int Convoy::getNumOfSameTypeShips(int type) const
{
	if (type < 1 || type>3)
		throw invalid_argument("WRONG TYPE OF SHIP");

	int numOfSameType = 0;
	TableShipIterator it;
	for (it = table->begin(); it != table->end(); ++it)
	{
		if (type == (*it).ship->getTypeOfShip())
			++numOfSameType;
	}
	return numOfSameType;
}

Convoy& Convoy::includeNewShip(TableElem& newShip)
{
	if (!table)
		table = new TableShip;
	table->push_back(newShip);
	return *this;
}

Convoy& Convoy::excludeShip(string call_letter)
{
	if (!table)
		throw logic_error("The table is empty");
	if (!table->getNumOfShips())
		throw logic_error("The table is empty");
	table->deleteElem(call_letter);
	return *this;
}


Convoy&Convoy:: operator =(const Convoy& copy)
{
	table->erase();
	table = new TableShip;
	*table = *(copy.table);
	mainCap = copy.mainCap;
	from = copy.from;
	toward = copy.toward;
	distance = copy.distance;
	return *this;
}

Convoy& Convoy::modernCarrierToMilitary(string call_letter)
{
	bool find = false;
	TableShipIterator it;
	for (it = table->begin(); it != table->end(); ++it)
		if ((*it).call_letter == call_letter) {
			find = true;
			break;
		}
	if (!find)
		throw invalid_argument("SHIP WITH THIS CALL LETTER DOESN'T EXIST");
	if ((*it).ship->getTypeOfShip() != CARRIER)
		throw invalid_argument("SHIP IS NOT CARRIER");
	Carrier *carr = dynamic_cast<Carrier *>((*it).ship);
	MilitaryShip newShip(carr->getCaptain(), carr->getShipName(), carr->getMaxSpeed(), carr->getShipDisplacement(), carr->getNumPassengers(), carr->getCargoWeight(), 0);
	(*it).erase();
	(*it).ship = newShip.clone();
	if (mainCap->call_letter == (*it).call_letter)
	{
		delete mainCap;
		mainCap = new TableElem(*it);
	}
	try {
		moveWeaponry((*it).ship);
	}
	catch (logic_error &ia)
	{
		cout << ia.what() << endl;
	}
	return *this;
}


void Convoy::moveWeaponry(BaseShip* withoutWeaponryShip)
{
	bool find = false;
	TableShipIterator it, it1;
	size_t max = 0;
	for (it = table->begin(); it != table->end(); ++it)
		if ((*it).ship->getTypeOfShip() == MILITARY)
		{
			MilitaryShip *carr = dynamic_cast<MilitaryShip*>((*it).ship);
			if (carr->getNumGun() > max)
			{
				max = carr->getNumGun();
				it1 = it;
			}
			find = true;
		}
	if (!find)
		throw logic_error("THERE IS NO MILITARY OR GUARD SHIP");

	if (max == 0)
		throw logic_error("CONVOY DOES NOT HAVE WEAPONRY IN MILITARY SHIPS");

	if (max == 1)
	{
		throw logic_error("CONVOY DOES NOT HAVE EXCESS WEAPONRY IN MILITARY SHIPS");
	}
	else
	{
		MilitaryShip *ship = dynamic_cast<MilitaryShip *>((*it1).ship);
		MilitaryShip *shipneedweap = dynamic_cast<MilitaryShip *>(withoutWeaponryShip);
		for (size_t k = 0; k < max / 2; ++k) {
			Weaponryiterator wit = ship->begin();
			for (size_t ki = 0; ki < max / 2; ++ki)
				++wit;
			shipneedweap->addWeaponry(*wit);
			ship->deleteWeaponry();
		}
	}
}


Convoy& Convoy::redistCargo()
{
	double allCargo = 0;
	double numOfCargoShips = 0;
	bool find = false;
	TableShipIterator it;
	for (it = table->begin(); it != table->end(); ++it)
		if ((*it).ship->getTypeOfShip() != GUARD)
		{
			if ((*it).ship->getTypeOfShip() == CARRIER)
			{
				Carrier *carr = dynamic_cast<Carrier *>((*it).ship);
				allCargo += carr->getCargoWeight();
				++numOfCargoShips;
			}
			else {
				MilitaryShip *ship = dynamic_cast<MilitaryShip *>((*it).ship);
				allCargo += ship->getCargoWeight();
				++numOfCargoShips;
			}
			find = true;
		}
	if (!find)
		throw logic_error("THERE IS OLNLY GUARD SHIPS");

	double sameCargo = allCargo / numOfCargoShips;
	for (it = table->begin(); it != table->end(); ++it)
		if ((*it).ship->getTypeOfShip() != GUARD)
		{
			if ((*it).ship->getTypeOfShip() == CARRIER)
			{
				Carrier *carr = dynamic_cast<Carrier *>((*it).ship);
				carr->setCargoWeight(sameCargo);
			}
			else {
				MilitaryShip *ship = dynamic_cast<MilitaryShip *>((*it).ship);
				ship->setCargoWeight(sameCargo);
			}
		}

	return *this;
}


Convoy& Convoy::transCargo(string first, string second, double howCargo)
{
	bool find = false;
	TableShipIterator it1, it2;
	for (it1 = table->begin(); it1 != table->end(); ++it1)
		if ((*it1).call_letter == first) {
			if ((*it1).ship->getTypeOfShip() != GUARD)
			{
				find = true;
				break;
			}
		}
	if (!find)
		throw invalid_argument("SHIP WITH THIS CALL LETTER DOESN'T EXIST");

	if ((*it1).ship->getTypeOfShip() == CARRIER)
	{
		Carrier *ship = dynamic_cast<Carrier *>((*it1).ship);
		if (howCargo > ship->getCargoWeight())
			throw invalid_argument("ILLEGAL VALUE OF CARGO ");
		ship->setCargoWeight(ship->getCargoWeight() - howCargo);
	}
	else {
		MilitaryShip *ship = dynamic_cast<MilitaryShip *>((*it2).ship);
		if (howCargo > ship->getCargoWeight())
			throw invalid_argument("ILLEGAL VALUE OF CARGO ");
		ship->setCargoWeight(ship->getCargoWeight() - howCargo);
	}
	find = false;
	for (it2 = table->begin(); it2 != table->end(); ++it2)
		if ((*it2).call_letter == second) {
			if ((*it2).ship->getTypeOfShip() != GUARD)
			{
				find = true;
				break;
			}
		}
	if (!find)
		throw invalid_argument("SHIP WITH THIS CALL LETTER DOESN'T EXIST");

	if ((*it2).ship->getTypeOfShip() == CARRIER)
	{
		Carrier *ship = dynamic_cast<Carrier *>((*it2).ship);
		ship->setCargoWeight(ship->getCargoWeight() + howCargo);
	}
	else {
		MilitaryShip *ship = dynamic_cast<MilitaryShip *>((*it2).ship);
		ship->setCargoWeight(ship->getCargoWeight() + howCargo);
	}

	return *this;
}


void Convoy::setMainCap(string call_letter)
{
	if (!table)
		throw logic_error("The table is empty");
	if (!table->getNumOfShips())
		throw logic_error("The table is empty");
	TableShipIterator it;
	bool find = false;
	for (it = table->begin(); it != table->end(); ++it)
		if ((*it).call_letter == call_letter)
		{
			mainCap = new TableElem(*it);
			find = true;
			break;
		}
	if (!find)
		throw invalid_argument("THERE IS NOT SHIP WITH THIS CALL LETTER");
}




void Convoy::setFrom(string leave)
{
	if (leave == toward)
		throw invalid_argument("ERROR. PLACES CANNOT HAVE SAME NAMES");
	from = leave;
}



void Convoy::setToward(string arrive)
{
	if (arrive == from)
		throw invalid_argument("ERROR. PLACES CANNOT HAVE SAME NAMES");
	toward = arrive;
}

void Convoy::setDistance(double dist)
{
	if (dist == 0)
		throw invalid_argument("IMPOSSIBLE DISTANCE");
	distance = dist;
}

const TableElem Convoy::getCap() const
{
	return *mainCap;
}
string Convoy::getFrom() const
{
	return from;
}
string Convoy::getToward() const
{
	return toward;
}
double Convoy::getDistance() const
{
	return distance;
}

BaseShip* Convoy::getShip(string call_letter) const
{
	BaseShip* re;
	re = table->returnShip(call_letter);
	if (!re)
		throw invalid_argument("SHIP WITH THIS CALL LETTER DOES NOT EXIST");
	return re;

}
Carrier* Convoy::getCarrier(string call_letter) const
{
	BaseShip* res;
	res = table->returnShip(call_letter);
	if (res == NULL)
		throw invalid_argument("SHIP WITH CALL LETTER DOESN'T EXIST");
	if (res->getTypeOfShip() != CARRIER)
		throw invalid_argument("SHIP WITH CALL LETTER IS NOT CARRIER");
	Carrier *ship = dynamic_cast<Carrier *>(res);
	return ship;
}
GuardShip* Convoy::getGuardShip(string call_letter) const
{
	BaseShip* res;
	res = table->returnShip(call_letter);
	if (res == NULL)
		throw invalid_argument("SHIP WITH CALL LETTER DOESN'T EXIST");
	if (res->getTypeOfShip() != GUARD)
		throw invalid_argument("SHIP WITH CALL LETTER IS NOT GUARD");
	GuardShip *ship = dynamic_cast<GuardShip *>(res);
	return ship;
}
MilitaryShip* Convoy::getMilitaryShip(string call_letter) const
{
	BaseShip* res;
	res = table->returnShip(call_letter);
	if (res == NULL)
		throw invalid_argument("SHIP WITH CALL LETTER DOESN'T EXIST");
	if (res->getTypeOfShip() != MILITARY)
		throw invalid_argument("SHIP WITH CALL LETTER IS NOT MILITARY");
	MilitaryShip *ship = dynamic_cast<MilitaryShip *>(res);
	return ship;
}

void Convoy::print()
{
	cout << "INFORMATION ABOUT CONVOY:" << endl;
	cout << "POINT OF DEPARTURE: " << from << endl << "DESTINATION: " << toward << endl << "DISTANCE" << distance << endl << "AVERAGE SPEED" << getRealSpeed() << endl;
	cout << "MAIN CAPTAIN: " << endl << "CALL LETTER" << endl << mainCap->call_letter << endl;
	mainCap->ship->print();
	cout << endl;
	table->print();
	return;
}


void Convoy::calculateAverageSpeed()
{
	averageSpeed = 0;
	double num = 0;
	for (auto it = table->begin(); it != table->end(); ++it)
	{
		averageSpeed += (*it).ship->getMaxSpeed();
		++num;
	}
	averageSpeed /= num;
}

double Convoy::getAverageSpeed()
{
	return averageSpeed;
}

void Convoy::calculateRealSpeed()
{
	double num = 0;
	realSpeed = 0;
	for (auto it = table->begin(); it != table->end(); ++it)
	{
		realSpeed += (*it).ship->getPossibleMaxSpeed();
		++num;
	}
	realSpeed /= num;
}

double Convoy::getRealSpeed()
{
	calculateRealSpeed();
	return realSpeed;
}

//const bool Convoy::legalConvoy()
//{
//	for (auto it = table->begin(); it != table->end(); ++it)
//	{
//		if ((*it).ship->getTypeOfShip() == GUARD || (*it).ship->getTypeOfShip() == MILITARY)
//			return true;
//	}
//	return false;
//}

void Convoy::erase()
{
	if (table)
	{
		table->erase();
		delete table;
	}
}

void Convoy::savebin(FILE*f)
{
	table->savebin(f);
}

void Convoy::loadbin(FILE*f)
{
	table = new TableShip;
	table->loadbin(f);
}