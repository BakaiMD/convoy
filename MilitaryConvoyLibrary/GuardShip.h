#pragma once
#include "BaseShip.h"
#include "Weaponry.h"

enum TypeOfGun { UNIVERSAL = 0,AIR,WATER,LAND };

typedef vector<Weaponry>::iterator Weaponryiterator;
typedef vector<Weaponry>::const_iterator const_Weaponryiterator;
/*! \brief �������� ����� , ����������� ������ ��������� �����
\details � �������� ����� ����������� ���������� �  ������� � ����������� ����������*/
class GuardShip :
	public BaseShip
{
public:
	/*! \brief ������� ����������� , ������ BaseShip*/
	GuardShip();
	/*! \brief ���������������� ����������� , ������ BaseShip*/
	GuardShip(Captain cap, string name, double speed, double displacement, int passengers,int k);
	~GuardShip();
	/*! \brief ���������� �������� , ����������� �� ������ ������ �����
	\return vector<Weaponry>::iterator*/
	Weaponryiterator begin();
	/*! \brief ���������� �������� , ����������� �� ����� ������ ���������� �����
	\return vector<Weaponry>::iterator*/
	Weaponryiterator end();
	/*! \brief ���������� ���������� �� ����������� ������
	\param ��� ������ � ��� ������������ �� �����
	\throw �������������� ���� ������ � ������ ������� ��� � �����*/
	Weaponry InfoAboutWeaponry(int type, char place) const;
	/*! \brief ���������� ���������� ������ �� ����� */
	unsigned int getNumGun() const;
	/*! \brief ��������� ������ � �������� ������ ���������� �����
	\param ������  ������ Weaponry*/
	GuardShip& addWeaponry(const Weaponry&);
	/*! \brief ��������� c�������� ������ � �������� ������ ���������� �����*/
	GuardShip& addWeaponry();
	/*! ������� ������ �� ������ ����������, ���� ��� �����������*/
	GuardShip& deleteWeaponry();
	/*! \brief ������� �� ���������������� ������ , ���������� ���������� ����������� �� ����
	\param ��� ������ � ��� ������������ �� �����
	\throw �������������� ���� ������ � ������ ������� ��� � �����*/
	GuardShip& FireShot(int _name , char _place);
	/*! \throw ���� ��������������� �������� max > ������������ ��������� �������� ������������� �����*/
	virtual void setMaxPossibleSpeed(double max);
	virtual void  print() const;
	virtual int getTypeOfShip() const;
	virtual ostream& writeln(ostream&);
	virtual istream& readln(istream&);
	virtual GuardShip & operator =(GuardShip &obj);
	GuardShip* clone() const;
	virtual void savebin(FILE *f);
	virtual void loadbin(FILE*f);
private:
	string TypeOfWeaponry[4] = { "UNIVERSAL","AIR","WATER","LAND" };
	char PlaceOfWeaponry[4] = { 'L','R','B','F' };
	vector<Weaponry> Ship_Weaponry;
};

