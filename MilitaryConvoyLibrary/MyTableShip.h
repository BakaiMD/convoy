#pragma once
#include "MyVector.h"
#include "TableElem.h"

typedef MyVector<TableElem>::iterator TableShipIterator;
typedef MyVector<TableElem>::const_iterator const_TableShipIterator;

class TableShip
{
public:
	TableShip();
	TableShip(const TableElem&);
	TableShip(TableShip&);
	~TableShip();
	TableShipIterator begin();
	TableShipIterator end();
	BaseShip* returnShip(string) const;
	int getNumOfShips() const;
	TableShip& push_back(const TableElem&);
	TableShip& deleteElem(string);
	TableShip& operator =(TableShip&);
	void erase();
	void print() const;
	void savebin(FILE*f);
	void loadbin(FILE*f);
private:
	MyVector<TableElem> vectorOfElements;
};

