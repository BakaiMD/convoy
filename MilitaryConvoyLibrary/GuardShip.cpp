#include "stdafx.h"
#include "GuardShip.h"
#include "BaseShip.h"
#include <iostream>
#include <time.h>
#include <vector>



using std::cout;
using std::cin;
using std::endl;


GuardShip::GuardShip():BaseShip()
{
	setMaxPossibleSpeed(0);
}

GuardShip::GuardShip(Captain cap, string name, double speed, double displacement, int passengers,int k ) :
	BaseShip(cap, name, speed, displacement, passengers)
{
	setMaxPossibleSpeed(getMaxSpeed());
	
	for (int i = 0; i < k; ++i)
		addWeaponry();
}

GuardShip::~GuardShip()
{
}

Weaponryiterator GuardShip ::begin()
{
	return Ship_Weaponry.begin();
}

Weaponryiterator GuardShip::end()
{
	return Ship_Weaponry.end();
}

void GuardShip:: setMaxPossibleSpeed(double PossibleSpeed)
{
	if (PossibleSpeed <= getMaxSpeed())
		setSpeed(PossibleSpeed);
}

void GuardShip::print() const
{
	if(getTypeOfShip()==GUARD)
	cout << "GUARD SHIP" << endl;
	BaseShip::print();
	vector<Weaponry>::const_iterator it;
	cout << "INFORMATION ABOUT SHIP WEAPONRY" << endl;
	if (Ship_Weaponry.empty())
		cout << "SHIP WEAPONRY IS EMPTY";
	else
	for (it = Ship_Weaponry.begin(); it != Ship_Weaponry.end(); ++it)
		cout << TypeOfWeaponry[it->typeOfGun] << " " << it->place << " " << it->caliber << " " << it->cannon_shot << " " << it->ammunition << endl;
}

unsigned int GuardShip::getNumGun() const
{
	return Ship_Weaponry.size();
}

Weaponry GuardShip::InfoAboutWeaponry(int type, char place) const
{
	vector<Weaponry>::const_iterator it;
	if (Ship_Weaponry.size() == 0)
		throw "Ship weaponry is empty";
	for (it = Ship_Weaponry.begin(); it != Ship_Weaponry.end(); it++)
	{
		if (it->typeOfGun == type && it->place == place)
			return *it;
	}
	throw "There is no gun with this name and place";
}

GuardShip& GuardShip::addWeaponry(const Weaponry&newGun)
{
	Ship_Weaponry.push_back(newGun);
	return *this;
}

GuardShip& GuardShip:: addWeaponry()
{
	Ship_Weaponry.reserve(Ship_Weaponry.size() + 1);
	int rnd;
	Weaponry gun;
	rnd = rand() % 4;
	gun.typeOfGun = rnd;
	rnd = rand() % 4;
	gun.place = PlaceOfWeaponry[rnd];
	rnd = rand() % 3974 + 26;
	gun.caliber = rnd;
	double rnd1;
	rnd1 = rand() % 10000 + 100;
	gun.cannon_shot = rnd1;
	rnd = rand() % 100 + 100;
	gun.ammunition = rnd;
	Ship_Weaponry.push_back(gun);
	return *this;
}

GuardShip& GuardShip::deleteWeaponry()
{
	Ship_Weaponry.pop_back();
	return *this;
}

GuardShip& GuardShip::FireShot(int type, char place)
{
	vector<Weaponry>::iterator it;
	if (Ship_Weaponry.empty())
		throw "Ship weaponry is empty";
	for (it = Ship_Weaponry.begin(); it != Ship_Weaponry.end(); it++)
	{
		if (it->typeOfGun == type && it->place == place)
			if (it->ammunition - 1 >= 0)
				--(it->ammunition);
			else
				throw logic_error("THIS WEAPONRY'S AMMUNITION  IS EMPTY");
	}
	return *this;
}


ostream& GuardShip:: writeln(ostream&os)
{
	BaseShip::writeln(os);
	setMaxPossibleSpeed(getMaxSpeed());
	vector<Weaponry>::iterator it;
	for (it = Ship_Weaponry.begin(); it != Ship_Weaponry.end(); ++it)
	{
		os << TypeOfWeaponry[it->typeOfGun] << endl;
		os << it->place << endl;
		os << it->cannon_shot << endl;;
		os << it->caliber << endl;
		os << it->ammunition << endl;
	}
	return os;
}

istream& GuardShip::readln(istream&is)
{
	int numOfGuns;
	BaseShip::readln(is);
	is >> numOfGuns;
	if (numOfGuns < 0)
		is.setstate(is.failbit);
	for (int i = 0; i < numOfGuns; ++i)
	{
		addWeaponry();
		/*bool rightPlace = false;
		Weaponry gun;
		is >> gun.typeOfGun;
		for (int i = 0; i < 4; ++i)
			if (gun.typeOfGun >3 && gun.typeOfGun <0)
			is.setstate(is.failbit);
		is >> gun.place;
		gun.place = toupper((unsigned char)gun.place);
		for (int i = 0; i < 4;++i)
			if (gun.place == PlaceOfWeaponry[i])
			{
				rightPlace = true;
				break;
			}
		if (!rightPlace)
			is.setstate(is.failbit);
		is >> gun.cannon_shot;
		if(gun.cannon_shot<10000)
			is.setstate(is.failbit);
		is >> gun.caliber;
		if(gun.caliber<26)
			is.setstate(is.failbit);
		is >> gun.ammunition;
		if(gun.ammunition<0)
			is.setstate(is.failbit);*/
	}
	return is;
}



GuardShip& GuardShip::operator=(GuardShip& obj)
{
	Weaponryiterator it;
	BaseShip::operator=(obj);
	Ship_Weaponry.clear();
	for (it = obj.begin(); it != obj.end(); ++it)
	{
		Ship_Weaponry.push_back(*it);
	}
	return *this;
}

int GuardShip::getTypeOfShip() const
{
	return GUARD;
}

GuardShip* GuardShip::clone() const
{
	return new GuardShip(*this);
}

void GuardShip::savebin(FILE *f)
{
	BaseShip::savebin(f);
	size_t lenght;
	lenght = Ship_Weaponry.size();
	fwrite(&lenght, sizeof(size_t), 1, f);
	for (auto it = Ship_Weaponry.begin(); it != Ship_Weaponry.end(); ++it)
	{
		fwrite(&it->typeOfGun, sizeof(int), 1, f);
		fwrite(&(it->place), sizeof(char), 1, f);
		fwrite(&(it->ammunition), sizeof(int), 1, f);
		fwrite(&(it->caliber), sizeof(int), 1, f);
		fwrite(&(it->cannon_shot), sizeof(double), 1, f);
	}
}

void GuardShip::loadbin(FILE*f)
{
	BaseShip::loadbin(f);
	size_t lenght;
	fread(&lenght, sizeof(size_t), 1, f);
	for (size_t i = 0; i < lenght; ++i)
	{
		Weaponry it;
			fread(&(it.typeOfGun), sizeof(int), 1, f);
		fread(&(it.place), sizeof(char), 1, f);
		fread(&(it.ammunition), sizeof(int), 1, f);
		fread(&(it.caliber), sizeof(int), 1, f);
		fread(&(it.cannon_shot), sizeof(double), 1, f);
		Ship_Weaponry.push_back(it);
	}
}