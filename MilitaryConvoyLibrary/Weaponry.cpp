#include "stdafx.h"
#include "Weaponry.h"


Weaponry::Weaponry()
{
	typeOfGun = 0;
	caliber = 0;
	cannon_shot = 0;
	ammunition = 0;
	place = 'U';
}

Weaponry::Weaponry(int type, int _caliber, double cannon, int _ammun, char _place)
{
	typeOfGun = type;
	caliber = _caliber;
	cannon_shot = cannon;
	ammunition = _ammun;
	place = _place;
}

Weaponry::~Weaponry()
{
}
