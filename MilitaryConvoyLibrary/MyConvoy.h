#pragma once

#include "BaseShip.h"
#include "GuardShip.h"
#include "BaseShip.h"
#include "MilitaryShip.h"
#include <iostream>
#include "MyTableShip.h"
class Convoy
{
public:
	Convoy();
	~Convoy();
	Convoy(const Convoy&);
	Convoy(TableShip *tabShip, string Cap, string leave, string arrive, double dist);
	int getNumOfSameTypeShips(int type) const;
	Convoy& operator =(const Convoy&);
	Convoy& includeNewShip(TableElem&);
	Convoy& excludeShip(string call_letter);
	Convoy& modernCarrierToMilitary(string call_letter);
	Convoy& redistCargo();
	Convoy& transCargo(string first, string second, double howCargo);
	void setMainCap(string call_letter);
	void setFrom(string leave);
	void setTable(TableShip* tab) { table = tab; };
	void setToward(string toward);
	void setDistance(double dist);
	TableShip* getTable() const { return table; };
	const TableElem getCap() const;
	string getFrom() const;
	string getToward() const;
	double getDistance() const;
	BaseShip* getShip(string call_letter) const;
	Carrier* getCarrier(string call_letter) const;
	GuardShip* getGuardShip(string call_letter) const;
	MilitaryShip* getMilitaryShip(string call_letter) const;
	void print();
	/*const bool legalConvoy();*/
	double getAverageSpeed();
	double getRealSpeed();
	void erase();
	void savebin(FILE*f);
	void loadbin(FILE*f);
private:
	void calculateAverageSpeed();
	void calculateRealSpeed();
	void moveWeaponry(BaseShip*);
	TableShip* table;
	TableElem* mainCap;
	string from;
	string toward;
	double distance;
	double averageSpeed;
	double realSpeed;
};
