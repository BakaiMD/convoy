#pragma once
#include <string>
#include "Captain.h"
using  std::string;
using namespace std;

/// ����� ��������� ����� �����
enum TypeOfShips { 
	CARRIER = 1,///< ���������, ��� ����� ����� ��� ������������
	MILITARY,///< ���������, ��� ����� ����� ��� �������
	GUARD///< ���������, ��� ����� ����� ��� ��������
};

/*! \brief ������������ ����� , ��������������� ��� �������� ����� \details ����� �������� � ���� ��������� ����������:

1.�������� �����

2.���������� � ��������

3.������������� �����

4.������������ ��������

5.���������� ������ �������*/
class BaseShip
{
public:
	/*! \brief ������� ����������� \details ��� ������ ���������� � ���� , �������� ����� � ������ ������*/
	BaseShip();
	/*! \brief ����������� �������� ������ ���������������� ���������� ����������	\param cap -  ������ ������ Captain - ���������� � �������� 
	
	name - ��� ����� 
	
	MAXspeed - ������������ �������� 
	
	displacement - ������������� ����� 
	
	passengers - ���������� ������ ������� */
	BaseShip(Captain cap,string name,double MAXspeed,double displacement, int passengers);
	
	/*! \brief ���������� �����������
	\param ������ ������ BaseShip */
	BaseShip(const BaseShip&);
	/*! \brief ����������*/
	~BaseShip();
	/* \brief getter: ���������� � ��������
	\return Captain
	*/
	Captain getCaptain() const;
	/* \brief setter: ������������� ���������� � ��������
	\param cap ������ Captain
	*/
	void setCaptain(const Captain& cap) ;
	/* \brief getter: ���������� � ������������ �������� �����
	\return double
	*/
	double getMaxSpeed() const;
	/* \brief setter: ������������� ���������� � ��������
	\param MAXspeed ��������������� �������� 
	\throw �������� �� ����� ���� �������������
	*/
	void setMaxSpeed(double MAXspeed);
	/* \brief getter: ���������� � ��������� ���� ��������
	\return double
	*/
	double getPossibleMaxSpeed() const;
	/* \brief getter: ���������� � �������� �����
	\return string
	*/
	string getShipName() const;
	/* \brief setter: ������������� �������� �����
	\param newName ����� ��������
	*/
	void setShipName(string newName) ;
	/* \brief getter: ���������� � ������������� �����
	\return double
	*/
	double getShipDisplacement() const;
	/* \brief setter: ������������� ���������� � ������������� �����
	\param double
	\throw �� ����� ���� �������������
	*/
	void setShipDisplacement(double) ;
	/* \brief getter: ���������� � ���������� ����������
	\return int
	*/
	int getNumPassengers() const;
	/* \brief setter: ������������� ���������� � ���������� ����������
	\param int
	*/
	void setNumPassengers(int);
	/* \brief getter: ���������� � ���� �������
	\return int (enum TypeOfShips)
	*/
	virtual int getTypeOfShip() const = 0;
	virtual ostream& writeln(ostream&);
	virtual istream& readln(istream&);
	/*! \brief ������������� �������� ������������*/
	virtual BaseShip & operator =(const BaseShip &obj);
	/*! \brief ������������� �������� ����� ������ ������ BaseShip � ��� ����������� , ��������������� ����� readln*/
	friend istream&operator >>(istream&, BaseShip&);
	/*! \brief ������������� �������� �s���� ������ ������ BaseShip � ��� ����������� , ��������������� ����� writeln*/
	friend ostream&operator <<(ostream&, BaseShip&);
	/*! \brief ������ ����� ������ ����������� ������ BaseShip*/
	virtual void print() const;
	/*! \brief ������� �����, ������� �� ���� �����*/
	virtual void setMaxPossibleSpeed(double max)=0;
	/*! \brief ������� ���� �������  \return���������� ������ �� ������ ����������� ������ BaseShip*/
	virtual BaseShip* clone()const=0 ;
	/*! \brief ���������� ������ ����� � �������� ��� ��������� ����*/
	virtual void savebin(FILE *f);
	/*! \brief �������� ������ ����� �� ��������� ��� ���������� �����*/
	virtual void loadbin(FILE*f);
	/*! \brief ��������� ����� ���� ����� (enum TypeOfShips) , ����� ���������� ���������� �������� � ����� loadbin
	\return int (enum TypeOfShips)*/
	int freadType(FILE*f);
private:
	Captain captain;
	string ship_name;
	double max_speed;
	double ship_displacement;
	int num_passengers;
	double speed;
protected:
	void setSpeed(double);
};

