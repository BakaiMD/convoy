#pragma once
#include "BaseShip.h"

/*! \brief �������� ����� ����������� ��������� ������������ �����
\details � �������� ����� ����������� ���������� � ����� �����*/
class Carrier :
	public BaseShip
{
public:
	/*! \brief ������� ����������� , ������ BaseShip*/
	Carrier();
	/*! \brief ���������������� ����������� , ������ BaseShip*/
	Carrier(Captain cap, string name, double speed, double displacement, int passengers, double cargo);
	/*! \brief ���������� ����������� , ������ BaseShip*/
	Carrier(const Carrier&);
	~Carrier();
	virtual Carrier* clone() const;
	double getCargoWeight() const;
	void setCargoWeight(double) ;
	/*! \throw ���� ��������������� �������� max > ������������ ��������� �������� ������������� �����*/
	virtual void setMaxPossibleSpeed(double max) ;
	/*! \brief ����� ����������� ����������� �������� ����� �� ��� ��������
	\return ����������� ��������� �������� ��� ������ ��������*/
	double lawOfMaxPossibleSpeed();
	virtual void  print() const;
	virtual int getTypeOfShip() const;
	virtual ostream& writeln(ostream&);
	virtual istream& readln(istream&);
	virtual Carrier & operator =(const Carrier &obj);
	virtual void savebin(FILE *f);
	virtual void loadbin(FILE*f);
private:
	double cargo_weight;
};

