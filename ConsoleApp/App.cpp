// App.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include "..\MilitaryConvoy\BaseShip.h"
#include "..\MilitaryConvoy\Carrier.h"
#include "..\MilitaryConvoy\GuardShip.h"
#include "..\MilitaryConvoy\MilitaryShip.h"
#include "..\MilitaryConvoy\MyTableShip.h"
#include "..\MilitaryConvoy\MyConvoy.h"
#include <iostream>
#include<string>
#include <ctime>
#include <vector>

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::vector;
char *getStr(std::istream &s)
{
	char buf[21];
	int n, len = 0, curLen;
	char *ptr = new char, *bptr;
	*ptr = '\0';
	s.get();
	do {
		s.get(buf, 21);
		if (s.bad()) {
			delete ptr;
			ptr = nullptr;
			s.clear();
			s.ignore(INT_MAX, '\n');
			throw "Failed to get string";
		}
		if (s.good()) {
			curLen = strlen(buf);
			len += curLen;
			bptr = ptr;
			try {
				ptr = new char[len + 1];
			}
			catch (std::bad_alloc &ba) {
				s.clear();
				s.ignore(INT_MAX, '\n');
				delete bptr;
				throw "Failed to allocate memory for string";
			}
			*ptr = '\0';
			strcat_s(ptr, len + 1, bptr);
			strcat_s(ptr, len + 1, buf);
			delete bptr;
		}
		else
			s.get();
	} while (s.good());
	s.clear();
	s.ignore(INT_MAX, '\n');
	return ptr;
}
int getNum(int &res, const char *msg)
{
	using namespace std;
	bool error = 0;
	const char *p = "";
	do {
		cout << p << "Please , enter the  " << msg << " ";
		cin >> res;
		if (!cin.good())
		{
			if (cin.fail())
			{
				cin.clear();
				cin.ignore();
				error = 1;
				p = "Error!! REPEAT....";
			}
			if (cin.bad())
				return -1;
		}
		else error = 0;
	} while (error);
	return 1;
}

int getNum(double &res, const char *msg)
{
	using namespace std;
	bool error = 0;
	const char *p = "";
	do {
		cout << p << "Please , enter the  " << msg << " ";
		cin >> res;
		if (!cin.good())
		{
			if (cin.fail())
			{
				cin.clear();
				cin.ignore();
				error = 1;
				p = "Error!! REPEAT....";
			}
			if (cin.bad())
				return -1;
		}
		else error = 0;
	} while (error);
	return 1;
}
int addingShips();
int initConvoy(Convoy& startConvoy);






const std::string Menu[] = { "1.Get information about ship ", "2. Change information about ship",
"3. Save table of ships", "4. Add ship",
"5. Delete ship from Convoy","6.Modern Carrier to Military","7.Transfer cargo","8.Redist cargo","9.Appoint new Captain", "10.Show all","0. Quit" },
Choice = "Make your choice",
Msg = "You are wrong; repeate please";
const string GETMenu[] = { "1.Get full information","2.Get information about" };
const string ChangeMenu[] = { "1.Base","2.Carrier","3.Military ship","4.Guard ship","0.Quit" };
int Answer(const std::string alt[], int n);

int GET(Convoy&), CHANGE(Convoy&), ADD(Convoy&), DELETE(Convoy&), MODERN(Convoy&), TRANSFER(Convoy&), REDIST(Convoy&), NEWCAP(Convoy&), SHOWALL(Convoy&), SAVE(Convoy&), LOAD(Convoy&);

int(*Funcs[])(Convoy &) = { NULL,GET, CHANGE,SAVE, ADD,DELETE,MODERN,TRANSFER,REDIST,NEWCAP,SHOWALL};

int ChBase(Convoy&), ChCarrier(Convoy&), ChMilitary(Convoy&), ChGuard(Convoy&);
int(*changeF[])(Convoy&) = { NULL,ChBase,ChCarrier,ChMilitary,ChGuard };

const int Num = sizeof(Menu) / sizeof(Menu[0]);
const int NumCh = sizeof(ChangeMenu) / sizeof(ChangeMenu[0]);

int main()
{
	srand(time(0));
	Convoy convoy;
	int ind;
	//LOAD(convoy);
	initConvoy(convoy);
	try {
		while (ind = Answer(Menu, Num))
			Funcs[ind](convoy);
			std::cout << "That's all. Buy!" << std::endl;
	}
	catch (const std::exception &er)
	{

		std::cout << er.what() << std::endl;
	}
	convoy.erase();
	return 0;
}

int Answer(const std::string alt[], int n)
{
	int answer;
	std::string prompt = Choice;
	std::cout << endl << "What do you want to do:" << std::endl;
	for (int i = 0; i < n; i++)
		
			std::cout << alt[i].c_str() << std::endl;
			std::cout << prompt.c_str() << ": -> ";
			prompt = Msg;
			do {
				std::cin >> answer;
			if (!std::cin.good())
				throw std::exception("Error when number of choice was entered");
		} while (answer < 0 || answer >= n);
		std::cin.ignore(80, '\n');
		return answer;
}

int addingShips()
{
	cout << "\n=============== Start menu ==================" << endl;
	cout << "1- Add Carrier\t 3- Add Guard Ship" << endl;
	cout << "2- Add  Military Ship\t 4- Exit" << endl;
	int mode;
	do {
		getNum(mode, "choise");
	} while (mode < 1 || mode >4);
	return mode;
}

int initConvoy(Convoy& startConvoy)
{
	bool exc = true;
	cout << "Convoy initialization:" << endl;
	string first;
	std::cout << "Enter a point departure:" << endl;;
	cin >> first;
	do {
		try
		{
			startConvoy.setFrom(first);
			exc = true;
		}
		catch (invalid_argument &ia)
		{
			cout << ia.what() << endl;
		}
	} while (!exc);
	string second;
	do {
		std::cout << "Enter destination:" << endl;
		cin >> second;
		try
		{
			startConvoy.setToward(second);
			exc = true;
		}
		catch (invalid_argument  &ia)
		{
			cout << ia.what() << endl;
			exc = false;
		}
	} while (!exc);
	double distance;
	getNum(distance, "distance between these points");
	try
	{
		startConvoy.setDistance(distance);
	}
	catch (invalid_argument &ia)
	{
		cout << ia.what() << endl;
		return 0;
	}
	int mode;
	cout << "1- Init Table" << endl << "2- Load Table" << endl;
	do {
		getNum(mode, "choise");
	} while (mode < 1 || mode>2);
	switch (mode)
	{
	case 1:{
		ADD(startConvoy);
		break;}
	case 2: {
		LOAD(startConvoy);
		break;
	}
	default:
		break;
	}
	string mainCap;
	do {
		std::cout << "Please, enter call letter captain of convoy:" << endl;
		cin >> mainCap;
		try {
			startConvoy.setMainCap(mainCap);
			exc = true;
		}
		catch (invalid_argument  &ia)
		{
			cout << ia.what() << endl;
			exc = false;
		}
		catch (logic_error  &ia)
		{
			cout << ia.what() << endl;
			exc = true;
		}
	} while (!exc);
	return 1;
}




int SHOWALL(Convoy& convoy)
{
	convoy.print();
	return 1;
}


int GET(Convoy& convoy)
{
	string calling;
	cout << "1- Get number of same type ships" << endl;
	cout << "2- Getting information about ship:" << endl;
	int mode;
	do {
		getNum(mode, "choise");
	} while (mode < 0 || mode>2);
	switch (mode)
	{
	case 1:
	{
		int type;
		cout << "1 -CARRIER , 2- MILITARY SHIP ,3- GUARD SHIP" << endl;
		do {
			getNum(type, "choise");
		} while (mode < 0 || mode>3);
		cout << convoy.getNumOfSameTypeShips(type)<<endl;
		break;
	}
	case 2: {
		cout << "Please,enter call letter of ship--> " << endl;
		cin >> calling;
		try
		{
			BaseShip* ship = convoy.getShip(calling);
			ship->print();
		}
		catch (invalid_argument &ia)
		{
			cout << ia.what() << endl;
		}
		break;
	}
	default:break;
	}
	return 1;
}

int CHANGE(Convoy& convoy)
{
	int ind;
	try {
		ind = Answer(ChangeMenu, NumCh);
			changeF[ind](convoy);
		std::cout << "That's all. Buy!" << std::endl;
	}
	catch (const std::exception &er)
	{
		std::cout << er.what() << std::endl;
	}
	return 0;
}

int ChBase(Convoy& convoy)
{
	string calling;
	cout << "Enter ship name--> " << endl;
	cin >> calling;
	try {
		BaseShip *ship;
		ship = convoy.getShip(calling);
		const string menu[] = { "1- Name of Ship", "2- Information of Captain","3- Ship displacement","4- Speed","5- number of passengers" "0- Quit"};
		const int num = sizeof(menu) / sizeof(menu[0]);
			switch (Answer(menu, num))
			{
			case 1:
			{
				string newName;
				cout << "Enter new ship name" << endl;
				cin >> newName;
				ship->setShipName(newName);
				break;
			}
			case 2:
			{
				cout << "Enter new information about captain" << endl;
				string first;
				cout << "Enter first name" << endl;
				cin >> first;
				string last;
				cout << "Enter last name" << endl;
				cin >> last;
				string capship;
				cout << "Enter captainship" << endl;
				cin >> capship;
				int exper;
				getNum(exper, "experience");
				Captain cap(first, last, capship, exper);
				ship->setCaptain(cap);
				break;
			}
			case 3:
			{
				double displ;
				getNum(displ, "Ship displacement");
				try {
					ship->setShipDisplacement(displ);
				}
				catch (invalid_argument &ia) 
				{
					cout << ia.what() << endl;
					return 0;
				};
				break;
			}
			case 4:
			{
				double speed;
				getNum(speed, "Speed of Ship");
				try {
					ship->setMaxPossibleSpeed(speed);
				}
				catch (invalid_argument &ia)
				{
					cout << ia.what() << endl;
					return 0;
				};
				break;
			}
			case 5:
			{
				int num;
				getNum(num, "number of passengers");
				try {
					ship->setNumPassengers(num);
				}
				catch (invalid_argument &ia)
				{
					cout << ia.what() << endl;
					return 0;
				};
				break;
			}
			case 0:
			{
				break;
			}
			default:
				break;
			}
		}
	catch (invalid_argument &ia)
	{
		cout << ia.what() << endl;
		return 0;
	};
	return 1;
}

int ChCarrier(Convoy& convoy)
{
	string calling;
	cout << "Enter ship name--> " << endl;
	cin >> calling;
	try {
		Carrier *ship = convoy.getCarrier(calling);
		const string menu[] = { "1- Name of Ship", "2- Information of Captain","3- Ship displacement","4- Speed","5- number of passengers","6- Cargo weight" "0- Quit" };
		const int num = sizeof(menu) / sizeof(menu[0]);
			switch (Answer(menu, num))
			{
			case 1:
			{
				string newName;
				cout << "Enter new ship name" << endl;
				cin >> newName;
				ship->setShipName(newName);
				break;
			}
			case 2:
			{
				cout << "Enter new information about captain" << endl;
				string first;
				cout << "Enter first name" << endl;
				cin >> first;
				string last;
				cout << "Enter last name" << endl;
				cin >> last;
				string capship;
				cout << "Enter captainship" << endl;
				cin >> capship;
				int exper;
				getNum(exper, "experience");
				Captain cap(first, last, capship, exper);
				ship->setCaptain(cap);
				break;
			}
			case 3:
			{
				double displ;
				getNum(displ, "Ship displacement");
				try {
					ship->setShipDisplacement(displ);
				}
				catch (invalid_argument &ia)
				{
					cout << ia.what() << endl;
					return 0;
				};
				break;
			}
			case 4:
			{
				double speed;
				getNum(speed, "Speed of Ship");
				try {
					ship->setMaxPossibleSpeed(speed);
				}
				catch (invalid_argument &ia)
				{
					cout << ia.what() << endl;
					return 0;
				};
				break;
			}
			case 5:
			{
				int num;
				getNum(num, "number of passengers");
				try {
					ship->setNumPassengers(num);
				}
				catch (invalid_argument &ia)
				{
					cout << ia.what() << endl;
					return 0;
				};
				break;
			}
			case 6:
			{double cargo;
			getNum(cargo, "Cargo weight of ship");
			try {
				ship->setCargoWeight(cargo);
			}
			catch (invalid_argument &ia)
			{
				cout << ia.what() << endl;
				return 0;
			};
			break;

			}
			case 0:
			{
				break;
			}
			default:
				break;
			}
		}
	catch (invalid_argument &ia)
	{
		cout << ia.what() << endl;
		return 0;
	};
	return 1;
}

int ChMilitary(Convoy& convoy)
{
	string calling;
	cout << "Enter ship name--> " << endl;
	cin >> calling;
	try {
		MilitaryShip *ship = convoy.getMilitaryShip(calling);
		const string menu[] = { "1- Name of Ship", "2- Information of Captain","3- Ship displacement","4- Speed","5- number of passengers","6- Cargo weight","7- Add weaponry" ,"0- Quit" };
		const int num = sizeof(menu) / sizeof(menu[0]);
			switch (Answer(menu, num))
			{
			case 1:
			{
				string newName;
				cout << "Enter new ship name" << endl;
				cin >> newName;
				ship->setShipName(newName);
				break;
			}
			case 2:
			{
				cout << "Enter new information about captain" << endl;
				string first;
				cout << "Enter first name" << endl;
				cin >> first;
				string last;
				cout << "Enter last name" << endl;
				cin >> last;
				string capship;
				cout << "Enter captainship" << endl;
				cin >> capship;
				int exper;
				getNum(exper, "experience");
				Captain cap(first, last, capship, exper);
				ship->setCaptain(cap);
				break;
			}
			case 3:
			{
				double displ;
				getNum(displ, "Ship displacement");
				try {
					ship->setShipDisplacement(displ);
				}
				catch (invalid_argument &ia)
				{
					cout << ia.what() << endl;
					return 0;
				};
				break;
			}
			case 4:
			{
				double speed;
				getNum(speed, "Speed of Ship");
				try {
					ship->setMaxPossibleSpeed(speed);
				}
				catch (invalid_argument &ia)
				{
					cout << ia.what() << endl;
					return 0;
				};
				break;
			}
			case 5:
			{
				int num;
				getNum(num, "number of passengers");
				try {
					ship->setNumPassengers(num);
				}
				catch (invalid_argument &ia)
				{
					cout << ia.what() << endl;
					return 0;
				};
				break;
			}
			case 6:
			{double cargo;
			getNum(cargo, "Cargo weight of ship");
			try {
				ship->setCargoWeight(cargo);
			}
			catch (invalid_argument &ia)
			{
				cout << ia.what() << endl;
				return 0;
			};
			break;

			}
			case 7:
			{
				ship->addWeaponry();
				break;
			}
			case 0:
			{
				break;
			}
			default:
				break;
			}
		}
		catch (invalid_argument &ia)
		{
			cout << ia.what() << endl;
			return 0;
		};
	return 1;
}


int ChGuard(Convoy& convoy)
{
	string calling;
	cout << "Enter ship name--> " << endl;
	cin >> calling;
	
	try {
		GuardShip *ship = convoy.getGuardShip(calling);
		const string menu[] = { "1- Name of Ship", "2- Information of Captain","3- Ship displacement","4- Speed","5- number of passengers","6- Add weaponry" ,"0- Quit" };
		const int num = sizeof(menu) / sizeof(menu[0]);
			switch (Answer(menu, num))
			{
			case 1:
			{
				string newName;
				cout << "Enter new ship name" << endl;
				cin >> newName;
				ship->setShipName(newName);
				break;
			}
			case 2:
			{
				cout << "Enter new information about captain" << endl;
				string first;
				cout << "Enter first name" << endl;
				cin >> first;
				string last;
				cout << "Enter last name" << endl;
				cin >> last;
				string capship;
				cout << "Enter captainship" << endl;
				cin >> capship;
				int exper;
				getNum(exper, "experience");
				Captain cap(first, last, capship, exper);
				ship->setCaptain(cap);
				break;
			}
			case 3:
			{
				double displ;
				getNum(displ, "Ship displacement");
				try {
					ship->setShipDisplacement(displ);
				}
				catch (invalid_argument &ia)
				{
					cout << ia.what() << endl;
					return 0;
				};
				break;
			}
			case 4:
			{
				double speed;
				getNum(speed, "Speed of Ship");
				try {
					ship->setMaxPossibleSpeed(speed);
				}
				catch (invalid_argument &ia)
				{
					cout << ia.what() << endl;
					return 0;
				};
				break;
			}
			case 5:
			{
				int num;
				getNum(num, "number of passengers");
				try {
					ship->setNumPassengers(num);
				}
				catch (invalid_argument &ia)
				{
					cout << ia.what() << endl;
					return 0;
				};
				break;
			}
			case 6:
			{ship->addWeaponry();
			break;

			}
			case 0:
			{
				break;
			}
			default:
				break;
			}
		}
	catch (invalid_argument &ia)
	{
		cout << ia.what() << endl;
		return 0;
	};
	return 1;
}

int ADD(Convoy& convoy)
{
	bool enough = true;
	while (enough)
	{
		switch (addingShips())
		{
		case 1:
		{
			Carrier ship;
			cout << "Carrier initialization:" << endl;
			int error, r;
			const char *p = "";
			do {

				cout << p << endl << "Please , enter the data in a specified format" << endl;
				cout << "Ship name <enter>" << endl;
				cout << "Captain: First name<enter> , Last name<enter>, captainShip<enter>, experience<enter> " << endl;
				cout << "Maximal speed<enter>, ship dicplacement<enter>, number of passengers<enter> ,cargo weight<enter>" << endl;
				cin >> ship;
				if (!cin.good())
				{
					if (cin.fail())
					{
						cin.clear();
						while (cin.get() != '\n');
						error = 1;
						p = "ERROR!! REPEAT....";
					}
					if (cin.bad())
						throw exception("FAIL ERROR!");
				}
				else error = 0;
			} while (error);
			bool exc = true;
			string call_letter;
			do {
				cout << "Call letter of ship--> ";
				cin >> call_letter;
				TableElem newElem(ship.clone(), call_letter);
				try {
					convoy.includeNewShip(newElem);
					exc = true;
				}
				catch (invalid_argument &ia)
				{
					cout << ia.what() << endl;
					newElem.erase();
					exc = false;
				}
			} while (!exc);
			break;
		}
		case 2:
		{
			MilitaryShip ship;
			cout << "Military Ship initialization:" << endl;
			int error, r;
			const char *p = "";
			do {
				cout << p << endl << "Please , enter the data in a specified format" << endl;
				cout << "Captain: First name<enter> , Last name<enter>, captainShip<enter>, experience<enter> " << endl;
				cout << "Maximal speed<enter>, ship dicplacement<enter>, number of passengers<enter> number of guns<enter>,cargo weight<enter>" << endl;
				cin >> ship;
				if (!cin.good())
				{
					if (cin.fail())
					{
						cin.clear();
						while (cin.get() != '\n');
						error = 1;
						p = "ERROR!! REPEAT....";
					}
					if (cin.bad())
						throw exception("FAIL ERROR!");
				}
				else error = 0;
			} while (error);
			bool exc = true;
			string call_letter;
			do {
				cout << "Call letter of ship--> ";
				cin >> call_letter;
				TableElem newElem(ship.clone(), call_letter);
				try {
					convoy.includeNewShip(newElem);
					exc = true;
				}
				catch (invalid_argument &ia)
				{
					cout << ia.what() << endl;
					newElem.erase();
					exc = false;
				}
			} while (!exc);
			break;
		}
		case 3:
		{GuardShip ship;
		cout << "Guard Ship initialization:" << endl;
		int error, r;
		const char *p = "";
		do {
			cout << p << endl << "Please , enter the data in a specified format" << endl;
			cout << "Captain: First name<enter> , Last name<enter>, captainShip<enter>, experience<enter> " << endl;
			cout << "Maximal speed<enter>, ship dicplacement<enter>, number of passengers<enter> number of guns<enter>" << endl;
			cin >> ship;
			if (!cin.good())
			{
				if (cin.fail())
				{
					cin.clear();
					while (cin.get() != '\n');
					error = 1;
					p = "ERROR!! REPEAT....";
				}
				if (cin.bad())
					throw exception("FAIL ERROR!");
			}
			else error = 0;
		} while (error);
		bool exc = true;
		string call_letter;
		do {
			cout << "Call letter of ship--> ";
			cin >> call_letter;
			TableElem newElem(ship.clone(), call_letter);
			try {
				convoy.includeNewShip(newElem);
				exc = true;
			}
			catch (invalid_argument &ia)
			{
				cout << ia.what() << endl;
				newElem.erase();
				exc = false;
			}
		} while (!exc);
		break;

		}
		case 4:
		{
			enough = false;
			break;
		}
		default:
			break;
		}
	}
	return 1;
}

int DELETE(Convoy& convoy)
{
	string calling;
	cout << "Enter name of deleting ship" << endl;
	std::getline(cin, calling);
	try { 
		bool cap=false;
		if (convoy.getCap().call_letter == calling)
			cap = true;
		convoy.excludeShip(calling);
		if (cap)
			NEWCAP(convoy);
	}
	catch (invalid_argument  &ia)
	{
		cout << ia.what() << endl;
	}
	catch (logic_error  &ia)
	{
		cout << ia.what() << endl;
	}
	return 1;
}

int MODERN(Convoy& convoy) 
{
	string calling;
	cout << "Enter call letter of carrier" << endl;
	cin >> calling;
	try {
		convoy.modernCarrierToMilitary(calling);
	}
	catch (invalid_argument &ia) {
		cout << ia.what() << endl;
	};
	return 1;
}

int REDIST(Convoy& convoy)
{
	try { convoy.redistCargo(); }
	catch (logic_error &le) { cout << le.what() << endl; }
	return 1;
}

int NEWCAP(Convoy& startConvoy)
{
	bool exc = true;
	string mainCap;
	do {
		std::cout << "Please, enter call letter captain of convoy:" << endl;
		cin >> mainCap;
		try {
			startConvoy.setMainCap(mainCap);
			exc = true;
		}
		catch (invalid_argument  &ia)
		{
			cout << ia.what() << endl;
			exc = false;
		}
		catch (logic_error  &ia)
		{
			cout << ia.what() << endl;
			exc = true;
		}
	} while (!exc);
	return 1;
}

int SAVE(Convoy& game)
{
	char*dir;
	FILE* f;
	std::cout << "Input file derictory: ";

	try {
		dir = getStr(std::cin);
	}
	catch (char*& what) {
		std::cout << what << std::endl;
		return 1;
	}
	fopen_s(&f, dir, "wb");
	game.savebin(f);
	fclose(f);
	return 0;
}

int LOAD(Convoy& game)
{
	char*dir;
	FILE* f;
	std::cout << "Input file derictory: ";
	try {
		dir = getStr(std::cin);
	}
	catch (char*& what) {
		std::cout << what << std::endl;
		return 1;
	}
	fopen_s(&f, dir, "rb");
	game.loadbin(f);
	fclose(f);
	return 0;
}

int TRANSFER(Convoy& convoy)
{
	string first, second;
	double height;
	cout << "Enter first ship-->  " ;
	cin>>first;
	cout << "Enter second ship-->  " ;
	cin>> second;
	getNum(height, "cargo weight");
	try {
		convoy.transCargo(first, second, height);
	}
	catch(invalid_argument &ia)
	{
		cout << ia.what() << endl;
	}
	return 1;

}
