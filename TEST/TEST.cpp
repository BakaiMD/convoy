// TEST.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include "..\MilitaryConvoy\BaseShip.h"
#include "..\MilitaryConvoy\Carrier.h"
#include "..\MilitaryConvoy\GuardShip.h"
#include "..\MilitaryConvoy\MilitaryShip.h"
#include "..\MilitaryConvoy\TableShip.h"
#include "..\MilitaryConvoy\Convoy.h"
#include <time.h>
#include "gtest\gtest.h"
#include <sstream>

TEST(CarrierTest, defaultConstructor)
{
	Carrier a;
	ASSERT_DOUBLE_EQ(CARRIER, a.getTypeOfShip())<<"����������� ���������� ��� �������";
	ASSERT_DOUBLE_EQ(0, a.getCargoWeight())<<"��� ����� ����������� ������";
	ASSERT_DOUBLE_EQ(0, a.getMaxSpeed()) << "�������� ����������� ����������������";
	/*ASSERT_DOUBLE_EQ(0, a.getNumPassengers()) << "���������� ���������� ������� ����������������";*/
	ASSERT_DOUBLE_EQ(0, a.getShipDisplacement()) << "������������� ����������� ������";
}

TEST(CarrierTest, setConstructor)
{

	Captain cap("name", "last", "capship", 10);
	for (int i = 0; i < 100; ++i) 
	{
		std::stringstream ss;
		string name = "name";
		string add;
		double speed = rand() % 1000 +1 ;
		double displace = rand() % 1000 + 1;
		int passen = rand() % 1000+ 1;
		ss << passen;
		ss >> add;
		name = name + add;
		double cargo= rand() % 1000 + 1;
		Carrier a(cap, "ship", speed, displace, passen,cargo );
		ASSERT_DOUBLE_EQ(CARRIER, a.getTypeOfShip()) << "����������� ���������� ��� �������";
		ASSERT_DOUBLE_EQ(cargo, a.getCargoWeight()) << "��� ����� ����������� ������";
		ASSERT_DOUBLE_EQ(speed, a.getMaxSpeed()) << "�������� ����������� ����������������";
		ASSERT_DOUBLE_EQ(passen, a.getNumPassengers()) << "���������� ���������� ������� ����������������";
		ASSERT_DOUBLE_EQ(displace, a.getShipDisplacement()) << "������������� ����������� ������";
		ASSERT_DOUBLE_EQ(10, a.getCaptain().experience);
		ASSERT_NO_THROW(a.getShipName())<< "��� ����� ���������������� �������";
	}
}


TEST(CarrierTest, CopyConstructor)
{
	Captain cap("name", "last", "capship", 10);
	for (int i = 0; i < 100; ++i)
	{
		std::stringstream ss;
		string name = "name";
		string add;
		double speed = rand() % 1000 + 1;
		double displace = rand() % 1000 + 1;
		int passen = rand() % 1000 + 1;
		ss << passen;
		ss >> add;
		name = name + add;
		double cargo = rand() % 1000 + 1;
		Carrier a(cap, "ship", speed, displace, passen, cargo), a1(a);
		ASSERT_DOUBLE_EQ(a.getTypeOfShip(), a1.getTypeOfShip()) << "����������� ���������� ��� �������";
		ASSERT_NO_THROW(a1.getShipName()) << "��� ����� ���������������� �������";
		ASSERT_NO_THROW(a1.getShipName()) << "��� ����� ���������������� �������";ASSERT_DOUBLE_EQ(a.getCargoWeight(), a1.getCargoWeight()) << "��� ����� ����������� ������";
		ASSERT_DOUBLE_EQ(a.getMaxSpeed(), a1.getMaxSpeed()) << "�������� ����������� ����������������";
		ASSERT_DOUBLE_EQ(a.getNumPassengers(), a1.getNumPassengers()) << "���������� ���������� ������� ����������������";
		ASSERT_DOUBLE_EQ(a.getShipDisplacement(), a1.getShipDisplacement()) << "������������� ����������� ������";
		ASSERT_DOUBLE_EQ(a1.getCaptain().experience, a.getCaptain().experience);
		
	}
}


TEST(CarrierTest, setterCARGOTest)
{
	Carrier a;
	for (int i = 0; i < 100; ++i)
	{
		double cargo = rand() % 100 - rand() % 100;
		if (cargo < 0)
			ASSERT_THROW(a.setCargoWeight(cargo), invalid_argument)<<"���������� ������������� �������������� �����";
		else {
			ASSERT_NO_THROW(a.setCargoWeight(cargo));
			ASSERT_DOUBLE_EQ(cargo, a.getCargoWeight()) << "��� ����� ����������� ������";
		}
	}
}

TEST(CarrierTest, setterNumTest)
{
	Carrier a;
	for (int i = 0; i < 100; ++i)
	{
		int cargo = rand() % 100 - rand() % 100;
		if (cargo < 0)
			ASSERT_THROW(a.setNumPassengers(cargo), invalid_argument) << "���������� ������������� �������������� �����";
		else {
			ASSERT_NO_THROW(a.setNumPassengers(cargo));
			ASSERT_DOUBLE_EQ(cargo, a.getNumPassengers()) << "��� ����� ����������� ������";
		}
	}
}


TEST(CarrierTest, setterDisplacementTest)
{
	Carrier a;
	for (int i = 0; i < 100; ++i)
	{
		double cargo = rand() % 100 - rand() % 100;
		if (cargo < 0)
			ASSERT_THROW(a.setShipDisplacement(cargo), invalid_argument) << "���������� ������������� �������������� �����";
		else {
			ASSERT_NO_THROW(a.setShipDisplacement(cargo));
			ASSERT_DOUBLE_EQ(cargo, a.getShipDisplacement()) << "��� ����� ����������� ������";
		}
	}
}

TEST(CarrierTest, setterSpeedTest)
{
	Carrier a;
		for (int i = 0; i < 100; ++i)
		{
			double speed = rand() % 100 - rand() % 100;
			if (speed < 0)
				ASSERT_THROW(a.setMaxSpeed(speed), invalid_argument) << "���������� ������������� �������������� ��������";
			else {
				ASSERT_NO_THROW(a.setMaxSpeed(speed));
				ASSERT_DOUBLE_EQ(speed, a.getMaxSpeed()) << "�������� ����������� ������";
				for (int i = 0; i < 100; ++i)
				{
					double cargo = rand() % 20000;
						ASSERT_NO_THROW(a.setCargoWeight(cargo));
						ASSERT_DOUBLE_EQ(cargo, a.getCargoWeight()) << "��� ����� ����������� ������";
						a.setMaxPossibleSpeed(a.getMaxSpeed());
						if (cargo <= 500)
							ASSERT_DOUBLE_EQ(a.getMaxSpeed(), a.getPossibleMaxSpeed())<<"����������� �������� ����� ����������� ��������";
						if (cargo > 500 && cargo <= 2000)
							ASSERT_DOUBLE_EQ(0.75*a.getMaxSpeed(), a.getPossibleMaxSpeed()) << "����������� �������� ����� ����������� ��������";
						if (cargo > 2000 && cargo <= 7000)
							ASSERT_DOUBLE_EQ(0.5*a.getMaxSpeed(), a.getPossibleMaxSpeed()) << "����������� �������� ����� ����������� ��������";
						if (cargo > 7000 && cargo <= 15000)
							ASSERT_DOUBLE_EQ(0.25*a.getMaxSpeed(), a.getPossibleMaxSpeed()) << "����������� �������� ����� ����������� ��������";
						if (cargo>15000)
							ASSERT_DOUBLE_EQ(0.1*a.getMaxSpeed(), a.getPossibleMaxSpeed()) << "����������� �������� ����� ����������� ��������";
					
				}
			}
		}
}



TEST(GuardTest, defaultConstructor)
{
	GuardShip a;
	ASSERT_DOUBLE_EQ(GUARD, a.getTypeOfShip()) << "����������� ���������� ��� �������";
	ASSERT_DOUBLE_EQ(0, a.getMaxSpeed()) << "�������� ����������� ����������������";
	/*ASSERT_DOUBLE_EQ(0, a.getNumPassengers()) << "���������� ���������� ������� ����������������";*/
	ASSERT_DOUBLE_EQ(0, a.getShipDisplacement()) << "������������� ����������� ������";
	ASSERT_DOUBLE_EQ(0, a.getNumGun());
}

TEST(GuardTest, setConstructor)
{

	Captain cap("name", "last", "capship", 10);
	for (int i = 0; i < 100; ++i)
	{
		std::stringstream ss;
		string name = "name";
		string add;
		double speed = rand() % 1000 + 1;
		double displace = rand() % 1000 + 1;
		int passen = rand() % 1000 + 1;
		int gun = rand() % 100;
		ss << passen;
		ss >> add;
		name = name + add;
		double cargo = rand() % 1000 + 1;
		GuardShip a(cap, "ship", speed, displace, passen, gun);
		ASSERT_DOUBLE_EQ(GUARD, a.getTypeOfShip()) << "����������� ���������� ��� �������";
		ASSERT_NO_THROW(a.getShipName()) << "��� ����� ���������������� �������";
		ASSERT_DOUBLE_EQ(gun, a.getNumGun()) << "��� ����� ����������� ������";
		ASSERT_DOUBLE_EQ(speed, a.getMaxSpeed()) << "�������� ����������� ����������������";
		ASSERT_DOUBLE_EQ(passen, a.getNumPassengers()) << "���������� ���������� ������� ����������������";
		ASSERT_DOUBLE_EQ(displace, a.getShipDisplacement()) << "������������� ����������� ������";
		ASSERT_DOUBLE_EQ(10, a.getCaptain().experience);
	}
}


TEST(GuardTest, CopyConstructor)
{
	Captain cap("name", "last", "capship", 10);
	for (int i = 0; i < 100; ++i)
	{
		std::stringstream ss;
		string name = "name";
		string add;
		double speed = rand() % 1000 + 1;
		double displace = rand() % 1000 + 1;
		int passen = rand() % 1000 + 1;
		ss << passen;
		ss >> add;
		name = name + add;
		int cargo = rand() % 1000 + 1;
		GuardShip a(cap, "ship", speed, displace, passen, cargo), a1(a);
		ASSERT_DOUBLE_EQ(a.getTypeOfShip(), a1.getTypeOfShip()) << "����������� ���������� ��� �������";
		ASSERT_NO_THROW(a1.getShipName()) << "��� ����� ���������������� �������";
		ASSERT_DOUBLE_EQ(a.getNumGun(), a1.getNumGun()) << "���������� ������ ����������� ������";
		ASSERT_DOUBLE_EQ(a.getMaxSpeed(), a1.getMaxSpeed()) << "�������� ����������� ����������������";
		ASSERT_DOUBLE_EQ(a.getNumPassengers(), a1.getNumPassengers()) << "���������� ���������� ������� ����������������";
		ASSERT_DOUBLE_EQ(a.getShipDisplacement(), a1.getShipDisplacement()) << "������������� ����������� ������";
		ASSERT_DOUBLE_EQ(a1.getCaptain().experience, a.getCaptain().experience);

	}
}

TEST(GuardTest, GunTest)
{
	GuardShip a;
	size_t num = rand() % 50 + 1;
	for (size_t i = 0; i < num; ++i)
	{
		a.addWeaponry();
	}
	ASSERT_DOUBLE_EQ(num, a.getNumGun());
}



TEST(GuardTest, FireShotTest)
{
	GuardShip a;
	a.addWeaponry();
	int ammun = a.begin()->ammunition;
	a.FireShot(a.begin()->typeOfGun, a.begin()->place);
	ASSERT_DOUBLE_EQ(ammun - 1, a.begin()->ammunition) << "�� ��������� �������";
	--ammun;
	for (int i = 0; i < ammun;++i)
		a.FireShot(a.begin()->typeOfGun, a.begin()->place) ;
	ASSERT_DOUBLE_EQ(0, a.begin()->ammunition) << "�������� � ������������";
}


TEST(GuardTest, DeleteGunTest)
{
	GuardShip a;
	int num = rand() % 20;
	size_t n = 0;
	for (int i = 0; i < num; ++i)
	{
		a.addWeaponry();
		++n;
	}
	ASSERT_DOUBLE_EQ(n, a.getNumGun());
	a.deleteWeaponry();
	--n;
	ASSERT_DOUBLE_EQ(n, a.getNumGun())<<"�� ��������� ������";
	for (int i = 0; i < num-1; ++i)
		a.deleteWeaponry();
	ASSERT_DOUBLE_EQ(0, a.getNumGun());
}

TEST(GuardTest, setterDisplacementTest)
{
	GuardShip a;
	for (int i = 0; i < 100; ++i)
	{
		double cargo = rand() % 100 - rand() % 100;
		if (cargo < 0)
			ASSERT_THROW(a.setShipDisplacement(cargo), invalid_argument) << "���������� ������������� �������������� �����";
		else {
			ASSERT_NO_THROW(a.setShipDisplacement(cargo));
			ASSERT_DOUBLE_EQ(cargo, a.getShipDisplacement()) << "��� ����� ����������� ������";
		}
	}
}

TEST(GuardTest, setterSpeedTest)
{
	GuardShip a;
	for (int i = 0; i < 100; ++i)
	{
		double speed = rand() % 100 - rand() % 100;
		if (speed < 0)
			ASSERT_THROW(a.setMaxSpeed(speed), invalid_argument) << "���������� ������������� �������������� ��������";
		else {
			ASSERT_NO_THROW(a.setMaxSpeed(speed));
			ASSERT_DOUBLE_EQ(speed, a.getMaxSpeed()) << "�������� ����������� ������";
		}
	}
}


TEST(MilitaryTest, defaultConstructor)
{
	MilitaryShip a;
	ASSERT_DOUBLE_EQ(MILITARY, a.getTypeOfShip()) << "����������� ���������� ��� �������";
	ASSERT_DOUBLE_EQ(0, a.getCargoWeight()) << "��� ����� ����������� ������";
	ASSERT_DOUBLE_EQ(0, a.getMaxSpeed()) << "�������� ����������� ����������������";
	/*ASSERT_DOUBLE_EQ(0, a.getNumPassengers()) << "���������� ���������� ������� ����������������";*/
	ASSERT_DOUBLE_EQ(0, a.getShipDisplacement()) << "������������� ����������� ������";
	ASSERT_DOUBLE_EQ(0, a.getNumGun());
}

TEST(MilitaryTest, setConstructor)
{

	Captain cap("name", "last", "capship", 10);
	for (int i = 0; i < 100; ++i)
	{
		std::stringstream ss;
		string name = "name";
		string add;
		double speed = rand() % 1000 + 1;
		double displace = rand() % 1000 + 1;
		int passen = rand() % 1000 + 1;
		ss << passen;
		ss >> add;
		name = name + add;
		double cargo = rand() % 1000 + 1;
		int num = rand() % 1000 + 1;
		MilitaryShip a(cap, "ship", speed, displace, passen, cargo,num);
		ASSERT_DOUBLE_EQ(MILITARY, a.getTypeOfShip()) << "����������� ���������� ��� �������";
		ASSERT_DOUBLE_EQ(cargo, a.getCargoWeight()) << "��� ����� ����������� ������";
		ASSERT_DOUBLE_EQ(speed, a.getMaxSpeed()) << "�������� ����������� ����������������";
		ASSERT_DOUBLE_EQ(passen, a.getNumPassengers()) << "���������� ���������� ������� ����������������";
		ASSERT_DOUBLE_EQ(displace, a.getShipDisplacement()) << "������������� ����������� ������";
		ASSERT_DOUBLE_EQ(10, a.getCaptain().experience);
		ASSERT_NO_THROW(a.getShipName()) << "��� ����� ���������������� �������";
		ASSERT_DOUBLE_EQ(a.getNumGun(),num) << "���������� ������ ����������� ������";
	}
}


TEST(MilitaryTest, CopyConstructor)
{
	Captain cap("name", "last", "capship", 10);
	for (int i = 0; i < 100; ++i)
	{
		std::stringstream ss;
		string name = "name";
		string add;
		double speed = rand() % 1000 + 1;
		double displace = rand() % 1000 + 1;
		int passen = rand() % 1000 + 1;
		ss << passen;
		ss >> add;
		name = name + add;
		double cargo = rand() % 1000 + 1;
		int num = rand() % 1000 + 1;
		MilitaryShip a(cap, "ship", speed, displace, passen, cargo,num), a1(a);
		ASSERT_DOUBLE_EQ(a.getTypeOfShip(), a1.getTypeOfShip()) << "����������� ���������� ��� �������";
		ASSERT_NO_THROW(a1.getShipName()) << "��� ����� ���������������� �������";
		ASSERT_NO_THROW(a1.getShipName()) << "��� ����� ���������������� �������"; ASSERT_DOUBLE_EQ(a.getCargoWeight(), a1.getCargoWeight()) << "��� ����� ����������� ������";
		ASSERT_DOUBLE_EQ(a.getMaxSpeed(), a1.getMaxSpeed()) << "�������� ����������� ����������������";
		ASSERT_DOUBLE_EQ(a.getNumPassengers(), a1.getNumPassengers()) << "���������� ���������� ������� ����������������";
		ASSERT_DOUBLE_EQ(a.getShipDisplacement(), a1.getShipDisplacement()) << "������������� ����������� ������";
		ASSERT_DOUBLE_EQ(a1.getCaptain().experience, a.getCaptain().experience);
		ASSERT_DOUBLE_EQ(a.getNumGun(), a1.getNumGun()) << "���������� ������ �� ���������";
	}
}


TEST(MilitaryTest, FireShotTest)
{
	MilitaryShip a;
	a.addWeaponry();
	int ammun = a.begin()->ammunition;
	a.FireShot(a.begin()->typeOfGun, a.begin()->place);
	ASSERT_DOUBLE_EQ(ammun - 1, a.begin()->ammunition) << "�� ��������� �������";
	--ammun;
	for (int i = 0; i < ammun; ++i)
		a.FireShot(a.begin()->typeOfGun, a.begin()->place);
	ASSERT_DOUBLE_EQ(0, a.begin()->ammunition) << "�������� � ������������";
}


TEST(MilitaryTest, setterDisplacementTest)
{
	MilitaryShip a;
	for (int i = 0; i < 100; ++i)
	{
		double cargo = rand() % 100 - rand() % 100;
		if (cargo < 0)
			ASSERT_THROW(a.setShipDisplacement(cargo), invalid_argument) << "���������� ������������� �������������� �����";
		else {
			ASSERT_NO_THROW(a.setShipDisplacement(cargo));
			ASSERT_DOUBLE_EQ(cargo, a.getShipDisplacement()) << "��� ����� ����������� ������";
		}
	}
}


TEST(MilitaryTest, setterSpeedTest)
{
	MilitaryShip a;
	for (int i = 0; i < 100; ++i)
	{
		double speed = rand() % 100 - rand() % 100;
		if (speed < 0)
			ASSERT_THROW(a.setMaxSpeed(speed), invalid_argument) << "���������� ������������� �������������� ��������";
		else {
			ASSERT_NO_THROW(a.setMaxSpeed(speed));
			ASSERT_DOUBLE_EQ(speed, a.getMaxSpeed()) << "�������� ����������� ������";
			for (int i = 0; i < 100; ++i)
			{
				double cargo = rand() % 20000;
				ASSERT_NO_THROW(a.setCargoWeight(cargo));
				ASSERT_DOUBLE_EQ(cargo, a.getCargoWeight()) << "��� ����� ����������� ������";
				a.setMaxPossibleSpeed(a.getMaxSpeed());
				if (cargo <= 1000)
					ASSERT_DOUBLE_EQ(a.getMaxSpeed(), a.getPossibleMaxSpeed()) << "����������� �������� ����� ����������� ��������";
				if (cargo > 1000 && cargo <= 5000)
					ASSERT_DOUBLE_EQ(0.75*a.getMaxSpeed(), a.getPossibleMaxSpeed()) << "����������� �������� ����� ����������� ��������";
				if (cargo > 5000 && cargo <= 10000)
					ASSERT_DOUBLE_EQ(0.5*a.getMaxSpeed(), a.getPossibleMaxSpeed()) << "����������� �������� ����� ����������� ��������";
				if (cargo > 10000 )
					ASSERT_DOUBLE_EQ(0.25*a.getMaxSpeed(), a.getPossibleMaxSpeed()) << "����������� �������� ����� ����������� ��������";
			}
		}
	}
}



int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(LC_ALL, "Russian");
	srand(time(NULL));
	::testing::InitGoogleTest(&argc, argv);
	int i = RUN_ALL_TESTS();
	system("pause");
	return i;
}